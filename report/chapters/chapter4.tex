\chapter{Algorithms for Tree Width}
\label{chapter4}

In this chapter we will introduce the foundations for studying algorithms related to Cops and Robbers and tree-width.

\section{Formalising Problems}
Loosely, a problem is a question we seek to answer, described by its parameters and the properties that a solution must satisfy. An instance of a problem is a specific question obtained through the specification of values for the problem parameters. A decision problem is a question that must be answered with "yes" or "no."

\begin{defn}
A \emph{decision problem} $\Pi$ is a set of instances $D_\Pi$, the domain of $\Pi$, consisting of a set of "yes-instances" $Y_\Pi \subseteq D_\Pi$ and a set of "no-instances" $N_\Pi = D_\Pi \setminus Y_\Pi$.
\end{defn}
\begin{defn}
An \emph{algorithm} is a specified sequence of steps taken to solve a problem given an initial input. An algorithm solves a decision problem $\Pi$ if, given an instance $I \in D_\Pi$, the algorithm determines if $I \in Y_\Pi$ or $I \in N_\Pi$.
\end{defn}

We want to describe the efficiency of an algorithm, that is, the number of steps that it must take before it terminates. We can do this by measuring the growth of the number of basic operations as a function of the size of the input instance. This function is a measure of the worst-case instances for each input size, i.e. the maximum time the algorithm would take to terminate for an instance of that size.

\begin{defn}
Given two functions $f: \mathbb{N} \to \mathbb{N}$ and $g: \mathbb{N} \to \mathbb{N}$, $f$ is $\mathcal{O}(g)$ if there exist $c \in \mathbb{Z}^+$ and $n_0 \in \mathbb{N}$ such that, for all $n \geq n_0$, $f(n) \leq c \cdot g(n)$.
\end{defn}
\begin{defn}
An algorithm $A$ (or the time complexity of an algorithm $A$) is $\mathcal{O}(g)$ for some function $g$ if $A$ performs $f(n)$ operations, in the worst case, on an input of size $n \in \nn$ and $f$ is $\bigoh(g)$.
\end{defn}
\begin{defn}
An algorithm $A$ is a \emph{polynomial time algorithm} if $A$ is $\mathcal{O}(p(n))$ for some polynomial function $p: \nn \to \nn$.
\end{defn}

We wish to speak about deterministic and non-deterministic algorithms, but the specific details are outside the scope of this report. We shall simply say that deterministic algorithms are algorithms that can be simulated by a ``basic-while-program'' on a Turing machine with a non-ambiguous grammar, and contrarily, a non-deterministic algorithm can be simulated by a "basic-while-program" on a Turing machine with an ambiguous grammar. Here, deterministic is taken, roughly, to mean that after completion of a step in the algorithm, there is only a single possible next step, i.e. there is no decision by the machine as to which step should be taken or parallelisation of steps, and vice versa for non-determinism. For greater depth, we refer to the works of Goldreich \cite{p-np}.

\section{Complexity Classes}
Here we describe the complexity classes $\mathbb{P}$ and $\mathbb{NP}$.

\begin{defn}
A \emph{certificate} of a problem $\Pi$ is an instance that is guessed to be in $Y_\Pi$.
\end{defn}
\begin{defn}
The \emph{complexity class} $\mathbb{P}$ is the class of problems solvable by a deterministic algorithm in polynomial time.
\end{defn}
\begin{defn}
The \emph{complexity class} $\mathbb{NP}$ is the class of problems $\Pi$ such that, given a certificate $c \in D_\Pi$, there is a non-deterministic algorithm that can verify that the certificate is in $Y_\Pi$ in polynomial time. Equivalently, $\mathbb{NP}$ is the class of problems solvable by a non-deterministic algorithm in polynomial time.
\end{defn}

\noindent Clearly, by these definitions, $\p \subseteq \np$, since any problem solvable, in polynomial time, by a deterministic algorithm can be solved similarly by a non-deterministic algorithm.

\begin{defn}
A \emph{polynomial transformation} from a decision problem $\Pi$ to a decision problem $\Psi$ is a function $f: D_\Pi \to D_\Psi$ such that $f$ is computable in polynomial time by a deterministic algorithm and for an instance $I \in D_\Pi$, $I \in Y_\Pi$ if and only if $f(I) \in Y_\Psi$. We write $\Pi \leq_m^p \Psi$ and say that $\Pi$ reduces to $\Psi$.
\end{defn}
\begin{defn}
The \emph{complexity class} of $\np$-hard problems is the class of problems $\Psi$ such that, for any problem $\Pi$ in $\np$, $\Pi \leq_m^p \Psi$. The complexity class of $\np$-complete problems is the class of $\np$-hard problems also in $\np$.
\end{defn}

\noindent The following decision problem is an $\np$-complete problem\cite{arnborg}:
\begin{center}
\problem{TW Tree-width}{A graph $G$ and $k \in \nn$.}{Does $G$ have tree-width at most $k$?}
\end{center}
That is, every decision problem in $\np$ is reducible to TW, i.e. TW is at least as hard as every other problem in $\np$. This is a significant barrier for us if we wish to study the tree-wdith of graphs in general. However, algorithms have been designed to exploit the structural properties of given graphs or to bound tree-width below a parameter of a certain form, and can even produce tree-decompositions with width of such a parameter. In particular Bodlaender produced a linear time algorithm that, for a fixed constant $k \in \nn$, given a graph $G$, determines whether the tree-width of $G$ is at most $k$, and if so, finds a tree-decomposition of $G$ with tree-width at most $k$ \cite{lin-time-algo}. It does this by partitioning the vertex set of a graph into vertices of ``high'' and ``low'' degree, then using the theory of matchings and I-simplicial vertices to prove that a related graph has bounded tree-width from which the final result may be derived. This is a complex result and certainly outside the scope of this report.

$\np$-complete problems such as TW are of particular algorithmic interest, since if it is possible to create a polynomial time algorithm to solve an $\np$-complete problem $\Pi$, that is we may add $\Pi$ to the more specific complexity class $\p$, then it is in fact the case that $\p = \np$. This is because, for any decision problem $\Psi \in \np$, we have $\Psi \leq_m^p \Pi$ since $\Pi$ is $\np$-complete. We confirm that this is true with the following results.

\begin{lem}
Let $f$ and $g$ be two functions computable in polynomial time. Then $g \circ f$ is computable in polynomial time. \label{compose_poly}
\end{lem}
\begin{proof}
Suppose $f$ is computable in time $\bigoh(n^p)$ and $g$ is computable in time $\bigoh(n^q)$, with $n$ the size of the input of $f$ and $g$, and $p,q \in \nn$. Then there exist $c_f \in \mathbb{Z}^+$ and $n_f \in \nn$ such that $f$ is computable in time $c_f \cdot n^p$ for inputs of size $n \geq n_f$. Similarly, there exist $c_g \in \mathbb{Z}^+$ and $n_g \in \nn$ such that $g$ is computable in time $c_g \cdot n^q$ for inputs of size $n \geq n_g$. Now choose $n_0 = \max\{n_f,n_g\}$, so the previous two statements still hold for inputs of size $n \geq n_0$. Then for an input $x \in \text{dom}(f)$ of size $n \geq n_0$, since we must compute $f(x)$ first and then $g(f(x))$, we find that $(g \circ f)(x) = g(f(x))$ is computable in time $c_f \cdot n^p + c_g(c_f \cdot n^p)^q \leq c_f \cdot n^{pq} + c_f^qc_g \cdot n^{pq} = (c_f + c_f^qc_g)n^{pq} = \bigoh(n^{pq})$ for $n \geq n_0$. Hence $g \circ f$ is computable in polynomial time.
\end{proof}

\begin{thm}
Given decision problems $\Pi$ and $\Psi$, if $\Psi \in \p$ and $\Pi \leq_m^p \Psi$, then $\Pi \in \p$.
\end{thm}
\begin{proof}
Let $A$ be an algorithm that solves $\Psi$ in polynomial time and let $f: D_\Pi \to D_\Psi$ be the polynomial time transformation from $\Pi$ to $\Psi$. Given $I \in D_\Pi$, we first compute $f(I)$ and then compute $A$ on $f(I)$, solving $\Pi$. This is done in polynomial time by Lemma \ref{compose_poly}, hence $\Pi \in \p$.
\end{proof}

So we see that if we can prove, for some $\np$-complete decision problem $\Pi$, that $\Pi \in \p$, then for any decision problem $\Psi \in \np$, $\Psi \in \p$. Thus we now have $\np \subseteq \p$ as well as $\p \subseteq \np$ as we established before. Hence $\p = \np$. This is, of course, yet to be proven or disproven, but such a proof would have serious implications for many graph theoretical problems, a large percentage of which are $\np$-hard in the general case.

\section{Separators}
Here we will describe the concept of separators of graphs and vertex sets and their relation to tree-width.
\begin{defn}
Given a graph $G = (V,E)$, a set $S \subseteq V$ is a \emph{separator} of $G$ if $G \setminus S$ has more components than $G$. If $S$ is a separator of $G$ then $S$ is said to separate $G$.
\end{defn}

\begin{defn}
Let $G = (V,E)$ be a graph, with a separator $S \subseteq V$ of $G$, and two sets $A \subseteq V$ and $B \subseteq V$. Then $S$ \emph{separates} $A$ from $B$ if, for any vertices $a \in A \setminus S$ and $b \in B \setminus S$, there is no walk from $a$ to $b$ (or $b$ to $a$) in $G \setminus S$.
\end{defn}

\begin{defn}
\label{sw} Let $G = (V,E)$ be a graph and $W \subseteq V$. A \emph{$W$-separator (balanced)} in $G$ is a set $S \subseteq V$ such that, for every $S$-flap, $C$, of $G$, $|C \cap W| \leq \frac{|W|}{2}$ holds. The \emph{separator width} of $G$, $\mathcal{SW}(G)$, is given by $$\mathcal{SW}(G) = \min\{n \in \nn: \text{for each $W \subseteq V$ there is a $W$-separator $S$ with $|S| \leq n$}\}.$$
\end{defn}

\begin{defn}
Let $G = (V,E)$ be a graph and $W \subseteq V$. A \emph{nearly balanced separation} of $W$ in $G$ is a triple $(X,S,Y)$ where $X \subseteq W$, $Y \subseteq W$, and $S \subseteq V$ such that: $S$ separates $X$ and $Y$; $W = X \cup (S \cap W) \cup Y$; $0<|X|<\frac{2}{3}|W|$ and $0<|Y|<\frac{2}{3}|W|$.
\end{defn}
\noindent This will allow us to describe an algorithm for creating tree-decompositions. The following two lemmas concerning tree-width are presented without proof (for proof see \cite{isolde}) and will allow us to bound the tree-width of a graph in the subsequent theorem.

\begin{lem}
Let $G = (V,E)$ be a graph with $\mathcal{W}(G) = k$ and $W \subseteq V$. Then there exists a $W$-separator $S \subseteq V$ such that $|S| \leq k+1$. \label{tw-lower}
\end{lem}

\begin{lem}
Let $k \in \nn$ and $G = (V,E)$ be a graph where, for all $W \subseteq V$ such that $|W| = 2k+1$, there is a $W$-separator $S$ in $G$ with $|S| \leq k$. Then $\mathcal{W}(G) \leq 3k$. \label{tw-upper}
\end{lem}

\noindent We can now bound the tree-width using separator width in the following theorem.

\begin{thm}
Let $G$ be a graph. Then $\mathcal{SW}(G) - 1 \leq \mathcal{W}(G) \leq 3\mathcal{SW}(G)$.\label{tw-sw-bound}
\end{thm}
\begin{proof}
Assume $\mathcal{W}(G) = n \in \nn$. Then by Lemma \ref{tw-lower}, given $W \subseteq V(G)$, there \linebreak is a $W$-separator, $S$, in $G$ with $|S| \leq n+1$. $\mathcal{SW}(G) \leq |S|$ by Definition \ref{sw}, so \linebreak $\mathcal{SW}(G) \leq |S| \leq n+1 = \mathcal{W}(G) + 1$. Hence, $\mathcal{SW}(G) - 1 \leq \mathcal{W}(G)$. We take \linebreak $k = \mathcal{SW}(G)$ in Lemma \ref{tw-upper} to attain the upper bound. Thus, we have \linebreak $\mathcal{SW}(G) - 1 \leq \mathcal{W}(G) \leq 3\mathcal{SW}(G)$.
\end{proof}
 
\section{The Separator Algorithm}
In this section we present an algorithm, based on separators, to construct tree-decompositions of a specific width. This algorithm executes the steps of the proof of Lemma \ref{tw-upper} presented in \cite{isolde}, in which it is shown that, for a graph $G$, if every $A \subseteq V(G)$ with $|A| = 2k+1$ has a balanced separator $S$ with $|S| \leq k$, then there is a tree-decomposition $(T,W)$ of $G$ such that $w(T,W) \leq 3k$ and $A = W_t \in W$ for some $t \in V(T)$. The algorithm in question, \textsc{Decomp}, is shown in the figure below.

\begin{center}
\begin{algorithm}
	\caption{\textsc{Decomp}}
	
	\begin{small}
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output}
	
	\Input{$(G,A,k)$, where $G$ is a graph, $k \in \nn$, $A \subseteq V(G)$ with $|A| \leq 3k+1$.}
	\Output{A tree-decomposition of width less than $4k+1$ or determines that $\mathcal{W}(G) > k$.}
	\BlankLine
	\Begin{
		\uIf {$e(G) > k \cdot v(G)$}{
			Return $\mathcal{W}(G) > k$		
		}
		\uElseIf {$v(G) \leq 4k+2$}{
			Return trivial tree-decomposition of G
		}
		\Else{
			Choose $A' \supseteq A$ with $|A'| = 3k+1$ \\
			\uIf {there exists a nearly balanced $A'$-separation $(X,S,Y)$ with $|S| \leq k+1$}{
				Define $C_1,...,C_m$ as $S$-flaps of $G$ \\
				\For {$i \leftarrow 1$ to $m$}{
					Define $G_i$	subgraph of $G$ induced by $(C_i \cup S)$ \\
					$A_i \leftarrow (C_i \cap A') \cup S$ \\
					$(T_i,W_i) \leftarrow \textsc{Decomp}(G_i,A_i,k)$
				}
				%Return $(T,W)$, where $T = (\{a,b,v_1,v_2,...,v_m\},\{ab,bv_1,bv_2,...,bv_m\})$ and $W = (W_a,W_b,W_{v_1},...,W_{v_m})$ with $W_a = A$, $W_b = A' \cup S$, and $W_{v_i} = A_i$ for $1 \leq i \leq m$
				$T \leftarrow (\bigcup_{i=1}^mV(T_i) \cup \{s,s'\}, \bigcup_{i=1}^mE(T_i) \cup \{ss'\} \cup \{st_i: t_i \in V(T_i)\text{ for }1 \leq i \leq m\})$ \\
				$W \leftarrow (\bigcup_{i=1}^mW_i \cup \{W_s = (A \cup S), W_{s'} = A\})$ \\
				Return tree-decomposition $(T,W)$
			}
			\Else{
				Return $\mathcal{W}(G) > k$
			}
		}
	}
	\end{small}
\end{algorithm}
\end{center}

To obtain a tree-decomposition of $G$ of width at most $4k+1$, we call $\textsc{Decomp}(G,\varnothing,k)$. If such a tree-decomposition does not exist, then the algorithm determines that $\mathcal{W}(G) > k$. We now prove a bound on the running time of this algorithm using a recurrence relation. First note that lines 2-5 run in time $\bigoh(k)$. Next we assume a worst case of $|S| = k+1$ within the algorithm and thus determine the $S$-flaps $C_1,...,C_m$ of $G$ as on line 11. We wish to choose the worst case for determining the vertices of $S$ and applying \textsc{Decomp} to each subgraph $G_i$ on line 13. We do this by choosing vertices of $S$ such that the operations performed by each call $\textsc{Decomp}(G_i,A_i,k)$ is maximised. In order to determine the running time of line 15, we need an additional theorem shown in \cite{isolde} and extending the work of Ford and Fulkerson in \cite{ford-fulkerson}, stated without proof:

\begin{thm}
Let $G$ be a graph, $k \in \nn$, and $W \subseteq V(G)$ such that $|W| = 3k+1$. If there exists a $W$-separator $S \subseteq V(G)$ with $|S| \leq k + 1$, then $S$ can be calculated in time $\bigoh(3^{3k} \cdot k \cdot e(G))$.
\end{thm}

Thus, we immediately conclude that line 15 runs in time $\bigoh(3^{3k} \cdot k \cdot e(G))$. Hence, we are able to formulate the recurrence relation $R$ as
\begin{align*}
R(v(G)) = \left\{
				\begin{array}{ll}
					\bigoh(k)& \quad \text{if $v(G) \leq 4k+2$} \\
					\max\{\sum_{i=1}^mR(v(G_i)): m \geq 2\} + \bigoh(3^{3k} \cdot k \cdot e(G))& \quad \text{otherwise}
				\end{array}
				\right.
\end{align*}

We have $\sum_{i=1}^m(v(G_i) - (k+1)) = v(G) - (k+1)$ since each subgraph $G_i$ is induced by $C_i \cup S$ with $C_i \cap S = \varnothing$. We now define a new relation $R'$ such that $R'(n) = R(n+k+1)$. So we have
\begin{footnotesize}
\begin{align*}
R'(v(G)) = &R(v(G)+k+1) \\
			 =	&\left\{
				\begin{array}{ll}
					\bigoh(k)& \quad \text{if $(v(G)+k+1) \leq 4k+2$} \\
					\max\{\sum_{i=1}^mR(v(G_i)+k+1): m \geq 2\} + \bigoh(3^{3k} \cdot k \cdot (e(G)+k+1))& \quad \text{otherwise}
				\end{array}
				\right. \\
			= &\left\{
				\begin{array}{ll}
					\bigoh(k)& \quad \text{if $v(G) \leq 3k+1$} \\
					\max\{\sum_{i=1}^mR(v(G_i)+k+1): m \geq 2\} + \bigoh(3^{3k} \cdot k \cdot (e(G)+k+1))& \quad \text{otherwise}
				\end{array}
				\right. \\
			= &\bigoh(3^{3k} \cdot k \cdot v(G)^2)
\end{align*}
\end{footnotesize}
Then we have $R(v(G)) = R'(v(G) - k - 1) = \bigoh(3^{3k} \cdot k \cdot v(G)^2)$ by the definition of $R'$. Hence, we conclude that \textsc{Decomp} runs in time $\bigoh(3^{3k} \cdot k \cdot v(G)^2)$. Since this bound is exponential in $k$, this should only be implemented when $k$ is very small. For example, taking $k=4$, we perform $\bigoh(3^{12} \cdot 4 \cdot v(G)^2)$ = $\bigoh(2125764 \cdot v(G)^2)$ operations, even without yet considering the number of vertices in the input graph. In the next section we will give an example of a faster algorithm, but one that is not guaranteed to result in a tree-decomposition of the input graph.

\section{$k$-good Tree-decompositions}
In this section we will define some properties of graphs that will be used to produce a further algorithm.

\begin{defn}
Let $G$ be a graph. The \emph{chordality} of $G$ is the length of the longest chordless cycle (a cycle with no chord) in $G$. $G$ is said to be \emph{$k$-chordal} if it has chordality $k$ for some $k \in \nn$.
\end{defn}

\begin{note}
A graph being ``chordal'' by Definition \ref{chordal-def} does not imply that it is $3$-chordal, since a chordal graph may have no cycles. The converse is true and follows directly from the definitions.
\end{note}

\begin{defn}
For integer $k > 1$, a \emph{$k$-caterpillar} is a graph $G$ such that there exists a set $P \subseteq V(G)$ inducing a path in $G$ of length $<k$ where, for every vertex $v \in V(G)$, either $v \in P$ or there exists $v' \in P$ such that $v \in N_G(v')$.
\end{defn}

Caterpillars are an important idea in the functionality of the algorithm we wish to create, so for clarity we illustrate the concept of caterpillars in the following example. They will be used to formulate a possible tree-decomposition to bound the tree-width of a graph, as will see in a later theorem.

\begin{ex}
$3$- and $5$-caterpillars, the former of which also happens to be a tree.

%Draw graphs
\begin{center}
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw,fill=black](A) {};
\draw (1,1) node[circle,draw,fill=black](B) {};
\draw (0,0) node[circle,draw](C) {};
\draw (0,2) node[circle,draw](D) {};
\draw (1,2) node[circle,draw](E) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (A) -- (C);
\draw (A) -- (D);
\draw (B) -- (E);
\end{tikzpicture}
\hspace{3cm}
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw,fill=black](A) {};
\draw (1,1) node[circle,draw,fill=black](B) {};
\draw (2,1) node[circle,draw,fill=black](C) {};
\draw (3,1) node[circle,draw,fill=black](D) {};
\draw (0.5,2) node[circle,draw](E) {};
\draw (0.5,0) node[circle,draw](F) {};
\draw (1.5,0) node[circle,draw](G) {};
\draw (3,0) node[circle,draw](H) {};
\draw (3,2) node [circle,draw](I) {};

%Edges
\draw (A) -- (B);
\draw (B) -- (C);
\draw (C) -- (D);
\draw (A) -- (E);
\draw (B) -- (E);
\draw (B) -- (F);
\draw (B) -- (G);
\draw (D) -- (H);
\draw (D) -- (I);
\end{tikzpicture}
\end{center}
\end{ex}

\begin{defn}
A tree-decomposition $(T,W)$ of a graph $G$ is \emph{$k$-good} if for all $W_t \in W$, the vertices in $W_t$ induce a $k$-caterpillar in $G$.
\end{defn}

As a matter of fact, if we can confirm that there exists a $k$-good tree-decomposition of a graph, then we can bound its tree-width from above. We confirm this with the subsequent proposition.

\begin{prop}
Let $G$ be a graph with a $k$-good tree-decomposition and $\Delta$ the maximum degree of any vertex in $G$. Then $\mathcal{W}(G) \leq (k-1)(\Delta-1)+2$. \label{k-good-bound}
\end{prop}
\begin{proof}
Let $(T,W)$ be a $k$-good tree-decomposition of $G$ for some $k > 1$. We will find an upper bound for the size of a bag in $(T,W)$ and, therefore, an upper bound on $w(T,W)$. For any set $W_t \in W$, $W_t$ induces a $k$-caterpillar. That is, $W_t$ contains an induced path $P$ of order $\leq k-1$. Suppose, towards an upper bound, there exists $W_u$, for some $u \in V(T)$, containing a path of order $k-1$. We must have, for any $v \in P$, $d_G(v) \leq \Delta$, so assume a worst case of $d_G(v) = \Delta$ for all $v \in P$. Then, the two endpoints of $P$ are adjacent to at most $\Delta-1$ vertices not in $P$. All other vertices of $P$ are adjacent to at most $\Delta-2$ vertices not in $P$. Thus it follows
\begin{align*}
|W_u| &\leq 2(\Delta-1) + (k-3)(\Delta-2) + (k-1) \\
		   &= 2\Delta - 2 + k\Delta - 2k - 3\Delta + 6 + k - 1 \\
		   &= k\Delta - k - \Delta + 3 \\
		   &= (k\Delta - k - \Delta + 1) + 2 \\
		   &= (k-1)(\Delta-1) + 2
\end{align*}
Hence we have
\begin{equation*}
\mathcal{W}(G) \leq w(T,W) \leq |W_u| \leq (k-1)(\Delta-1) + 2
\end{equation*}
\end{proof}
While this may seem a useful result upon first sight, it is in fact simply confirming an obvious fact - since we have an upper bound on the vertex degree and an upper bound on a specific set of vertices to which others must be adjacent, we can easily attain an upper bound on the size of a bag by multiplying these parameters in some sense. If we have already deduced that a graph admits a $k$-good tree-decomposition by finding an example, then it is likely that our previous result will not help us at all, since it only states the maximum width. Nevertheless, it is included to demonstrate rigor in our concepts. \\

We present the following theorem without proof. The extensive proof, attributed to Kosowski, Li, Nisse, and Suchan, can be seen in \cite{k-good-algo}.

\begin{thm}
Let $G$ be a graph and $k > 2$ an integer. Then there exists an algorithm that, given inputs $G$ and $k$, returns either a chordless cycle of length at least $k+1$ or a $k$-good tree-decomposition of $G$. \label{k-good-decomp}
\end{thm}

Using this, we may now make a simple deduction about the tree-width of $k$-chordal graphs:
\begin{cor}
Let $G$ be a $k$-chordal graph for integer $k > 1$ with maximum degree $\Delta$. Then $\mathcal{W}(G) \leq (k-1)(\Delta-1)+2$. \label{k-chordal-bound}
\end{cor}
\begin{proof}
Running the algorithm from Theorem \ref{k-good-decomp} returns either a chordless cycle of length at least $k+1$ in $G$ or a $k$-good tree-decomposition of $G$. Since $G$ is $k$-chordal, it contains no chordless cycle of length greater than $k$, so must have a $k$-good tree-decomposition. Thus, we get $\mathcal{W}(G) \leq (k-1)(\Delta-1)+2$ by Proposition \ref{k-good-bound}.
\end{proof}

\begin{ex}
We show the use of our corollary on the $3$-chordal graph below. The marked vertex has the highest degree of 6.

%Draw graph
\begin{center}
\begin{tikzpicture}

%Vertices
\draw (0,0) node[circle,draw](A) {};
\draw (0,2) node[circle,draw](B) {};
\draw (0,3) node[circle,draw](C) {};
\draw (1,1) node[circle,draw,fill=black](D) {};
\draw (1,3) node[circle,draw](E) {};
\draw (1,4) node[circle,draw](F) {};
\draw (2,0) node[circle,draw](G) {};
\draw (2,2) node[circle,draw](H) {};
\draw (3,1) node [circle,draw](I) {};

%Edges
\draw (A) -- (D);
\draw (B) -- (D);
\draw (G) -- (D);
\draw (H) -- (D);
\draw (I) -- (D);
\draw (D) -- (E);
\draw (E) -- (C);
\draw (E) -- (F);
\draw (C) -- (F);
\draw (A) -- (G);
\draw (G) -- (I);
\draw (I) -- (H);
\end{tikzpicture}
\end{center}

\noindent We determine from Corollary \ref{k-chordal-bound} that this graph has tree-width at most $2 \cdot 5 + 2 = 12$. From inspection we see that this graph is also chordal, and so we obtain a tree-width of 2 by Theorem \ref{chordal-tw-cliquenum}.\label{max-deg-example}
\end{ex}
Thus we see the value in different approaches to determining tree-width - it is unnecessary to apply lengthy algorithms to small graphs where one can easily derive the answer through inspection, and it is also imperative to choose the correct result to apply when deriving tree-width manually as one can obtain vastly different estimations depending on the structural qualities of the graph in question.

\section{$k$-good Algorithm}
We now formulate the algorithm \textsc{Decomp2} described in \cite{k-good-algo} and proven to exist in Theorem \ref{k-good-decomp} based upon k-good tree-decompositions. It is shown in the following figure. Note that on line 14 we assume the existence of a leaf bag $W_{t_i} \supset S_i$, which we may do by the proof of correctness by Kosowski, Li, Nisse, and Suchan.
We see that line 2 and lines 5-8 run in time $\bigoh(e(G))$ since we determine both the components of $G \setminus N_G(v)$ and bags $W_t$ by iterating edges. For the remainder of the algorithm, each pass of the loop on line 10 takes $\bigoh(e(G)^2)$ time and we perform $v(G)-1$ loops. So, for this formulation of the algorithm we obtain a time-complexity of $\bigoh(v(G)e(G)^2)$, but it is suggested in \cite{k-good-algo} that a faster implementation can be achieved.

\vspace{0.25cm}

\begin{algorithm}
	\caption{\textsc{Decomp2}}
	
	\begin{small}
	\SetKwInOut{Input}{input}
	\SetKwInOut{Output}{output}
	
	\Input{$(G,k)$, where $G$ is a graph and $k>2$ an integer.}
	\Output{A chordless cycle in $G$ of length $>k$ or a $k$-good tree-decomposition of $G$.}
	\BlankLine
	\Begin{
		Choose arbitrary $v \in V(G)$ and compute components $C_1,...C_j$ of $G \setminus N_G(v)$ \\
		$T \leftarrow (V(T),E(T)) = (\{t\},\varnothing)$ \\
		$W \leftarrow \{W_t\}$ where $W_t = N_G(v)$ \\
		\For {$i \leftarrow 1$ to $j$}{
			Add $t_i$ to $V(T)$ adjacent to $t$ \\
			Add $W_{t_i} = \{v\} \cup \{w \in N_G(v): N_G(w) \cap C_i \neq \varnothing\}$ to $W$
		}
		$G_0 \leftarrow \{v\}$ \\
		\For {$v' \in V(G) \setminus \{v\}$}{
			Compute components $C_1,...,C_\ell$ of $G \setminus G_0$ \\
			\For {$i \leftarrow 1$ to $\ell$}{
				$S_i \leftarrow \{u \in V(G_0): \text{$u$ adjacent to a vertex of $C_i$}\}$ \\
				$P_i \leftarrow W_{t_i} \setminus S_i$ \\
				Choose arbitrary $w \in S_i$ such that $Q = P_i \cup \{w\}$ is a chordless path \\
				Add a vertex $t'_i$ adjacent to $t_i$ in $T$ \\
				Add $W_{t'_i} = (Q \cup W_{t_i} \cup (N(w) \cap C_i))$ to $W$ \\
				\uIf{$|Q| > k$}{
					Return induced cycle $W_{t'_i}$ \\
				}
				\Else{
					Compute components $C'_1,...,C'_r$ of $(C_i \cup W_{t_i}) \setminus W_{t'_i}$ \\
					\For {$h \leftarrow 1$ to $r$}{
						$S'_h \leftarrow \{u \in S_i: \text{$u$ is adjacent to a vertex of $C'_h$}\}$ \\
						$Q_h \leftarrow $smallest subgraph of $Q$ such that every vertex of $S'_h$ is in $Q_h$ or is adjacent to a vertex in $Q_h$ \\
						Add vertex $t''_h$ adjacent to $t'_i$ in $T$ \\
						Add $W_{t''_h} = Q_h \cup S'_h$ to $W$
					}
				}
			}
			Add $v'$ to $G_0$
		}
		Return $k$-good tree-decomposition $(T,W)$
	}
	\end{small}
\end{algorithm}

\vspace{0.25cm}

To find a $k$-good tree-decomposition of a graph $G$ using this algorithm, it is required to run $\textsc{Decomp2}(G,k)$ with increasing values of $k$ until a success is found. However, unlike with the previous algorithm \textsc{Decomp}, \textsc{Decomp2} does not determine if the tree-width of a graph is greater than our chosen parameter, and so it may be required to first find the upper limit at which to halt. Since it may be the case that, for any given $k$, our input graph $G$ does indeed satisfy $\mathcal{W}(G) \leq k$ but does not have a $k$-good tree-decomposition, using this algorithm to estimate tree-width may lead to an overestimation.

We now briefly discuss how these algorithms compare. The running time of \textsc{Decomp} is exponential in $k$, so, as discussed before, it is inefficient to run this algorithm for large values of $k$. Contrarily, the running time of \textsc{Decomp2} does not depend on $k$, and so higher values of $k$ do not increase the running time of this algorithm. Thus it is perhaps more efficient to run \textsc{Decomp2} instead of \textsc{Decomp} for high values of $k$. However, as previously stated, \textsc{Decomp2} may not even produce a tree-decomposition of the input graph - so it is recommended, to save running \textsc{Decomp2} an excessive number of times, that \textsc{Decomp2} is run only on graphs that have already been determined to have a $k$-good tree-decomposition or \textsc{Decomp2} is only run up to a pre-determined bound on $k$ derived from inspection of the graph. Of course, if a $k$-good tree-decomposition is required, then \textsc{Decomp2} is the preferable option, since \textsc{Decomp} is not guaranteed to produce such a tree-decomposition. In contrast to high values of $k$, we may also consider high values of $v(G)$ and $e(G)$ for an input graph $G$. Since for a simple graph $G$ we have $e(G) \leq v(G)^2$ by Theorem \ref{bound_edges}, we obtain a running time of $\bigoh(v(G)^5)$ for \textsc{Decomp2}. So, for low values of $k$ and high values of $v(G)$ or $e(G)$, we see that it is better to choose \textsc{Decomp}. Specifically we require $3^{3k} \cdot k \leq v(G)^3$ for \textsc{Decomp} to be the better choice.