\chapter{Introduction}
\label{chapter1}

\section{Planning and Goals}
Before we begin with the main content of the report, we outline and plan what we wish to achieve in this document.

\subsection{Objectives}
The goal of this project is to present an understanding and formulation of the common ``cops and robbers'' combinatorial game played on a graph, including the theoretical structures and definitions involved, the proceedings of the game itself as played by 2 opponents, and the implications that various structural qualities of the graph can have on the game. This report will detail the findings of the author's research into graph structures and algorithms concerning the game. The project is fully theoretical, in that it involves no implementation, and will hence replace such a deliverable with detailed theoretical results herein. The core outcomes for this project are as follows:-
\begin{enumerate}
\item To present a thorough mathematical understanding of fundamental concepts in graph theory relating to the problem;
\item To construct an expository report recounting research into the game of ``cops and robbers;''
\item To evaluate the final project based on expected outcomes.
\end{enumerate}

\noindent These objectives will be addressed and fulfilled by the following deliverables of this project:
\begin{enumerate}
\item Explanations and examples of the studied game;\label{itm:deliv1}
\item An exposition of basic theory and advanced research materials related to ``cops and robbers;''\label{itm:deliv2}
\item Presentation and explanation of the author's own proofs of theoretical results;\label{itm:deliv3}
\item A conclusion of the findings of this report and evaluative summary of the goals met.\label{itm:deliv4}
\end{enumerate}

\noindent The deliverables 1-4 are contained wholly within this report, and hence this document should be considered to constitute the project itself.

\begin{center}
\begin{figure}
\begin{ganttchart}[vgrid,hgrid]{1}{20}
\gantttitle{Week (2019)}{10} \gantttitle{Week (2020)}{10} \\
\gantttitle{2}{1} 
\gantttitle{3}{1}
\gantttitle{4}{1}
\gantttitle{5}{1}
\gantttitle{6}{1}
\gantttitle{7}{1}
\gantttitle{8}{1}
\gantttitle{9}{1}
\gantttitle{10}{1}
\gantttitle{11}{1}
\gantttitle{12}{1}
\gantttitle{13}{1}
\gantttitle{14}{1}
\gantttitle{15}{1}
\gantttitle{16}{1}
\gantttitle{17}{1}
\gantttitle{18}{1}
\gantttitle{19}{1}
\gantttitle{20}{1}
\gantttitle{21}{1} \\
\ganttbar{Planning \& goals}{1}{3} \\
\ganttbar{Concepts review}{2}{6} \\
\ganttbar{Game research}{6}{9} \\
\ganttbar{Related material research}{9}{15} \\
\ganttbar{Algorithmic research}{16}{20}
\end{ganttchart}
\caption{The planned schedule for this project.}
\label{gantt}
\end{figure}
\end{center}

\subsection{Schedule}
Fortnightly supervision meetings are scheduled to aid progress and guidance with this project. Shown in figure \ref{gantt} is a Gantt chart to outline the task scheduling and time allocation. The final week will be used for evaluating the project. The allocation of time to each area is approximate and may deviate from the planned schedule due to various circumstances that arise during the course of the project, for example difficulty of literature or deviations in research topics. In this instance, ``research'' entails both the reading and exposition of mathematical literature, as well as the construction of theorems and original proofs.

\subsection{Ethical Considerations}
Due to the theoretical nature of this project, no consent for user testing, data collection, or photography and imaging is required. No personal data or transcripts - other than the names and institutions of cited academics - are obtained for this project and hence we need not consider the privacy and security of sensitive information. Where individuals involved with this project, for example supervisor and assessor, wish to remain anonymous or have their input unspecified, these arrangements are made. Clearly, since this document involves the citation of multiple academic articles, plagiarism of academic work is a major ethical issue for this project. In order to ensure that this document nor the author are guilty of academic misconduct through plagiarism, all material - for example theorems, concepts, proofs - not original to the author or considered ``common knowledge'' is properly and appropriately referenced, so as to credit the original source.

This consideration also constitutes the legal and professional issues concerning this project, since, though academic plagiarism is not understood to be illegal in itself, it could be considered fraudulent and result in a legal case if pursued. Academic plagiarism is not considered professional behavior and likely results in the removal of the author from their academic institution should it be committed. These points serve to exacerbate the need for proper referencing within this project.

To address another ethical and professional issue - where work not attributable to the author has been performed, for example giving advice and checking proofs, it is properly acknowledged and credit given. This is to ensure that the author is not given undue credit for the efforts of others.

\section{Fundamental Definitions}
We begin the main content of this report with some basic definitions on which the entirety of this project is based.
\begin{defn}A (\emph{undirected}) \emph{graph} $G$ is an ordered triple $(V(G),E(G),\psi_G)$, with \linebreak$V(G) = \{v_1,v_2,...,v_n\}$ being the set of \emph{vertices} of $G$, $E(G) = \{e_1,e_2,...,e_n\}$ the set of \emph{edges} of $G$ between any two vertices, and $\psi_G$ the ``incidence function'' of $G$. When it is clear we are considering the graph $G$, we often use $V$ and $E$ in place of $V(G)$ and $E(G)$ respectively. \end{defn}
\begin{defn}Given a graph $G = (V,E,\psi)$, $\psi$ is the \emph{incidence function} that associates an edge in $E$ with some unordered pair of (not necessarily distinct) vertices in $V$.\end{defn}
\begin{defn}Let $G = (V,E,\psi)$ be a graph. Then if, for some $e \in E$ and $u,v \in V$, we have $\psi(e) = \{u,v\}$: $e$ is said to \emph{join} $u$ and $v$; $e$ is \emph{incident} to both $u$ and $v$; $u$ and $v$ are \emph{endpoints} of $e$; $u$ and $v$ are \emph{adjacent} vertices. \end{defn}
\begin{note}We often omit $\psi_G$ from the definition of $G$ by simply having $E(G) \subseteq \binom{V(G)}{2}$ where, for $u,v \in V(G)$, $\{u,v\} \in E(G)$ if and only if $u$ and $v$ are adjacent in $G$. Further, we denote by $uv$ the unordered pair $\{u,v\}$.\end{note}
\begin{defn}Let $e$ be an edge in a graph $G$ with endpoints $u$ and $v$. Then $e$ is a \emph{loop} if and only if $u = v$. \end{defn}
\begin{defn}Let $G = (V,E)$ be a graph and $v \in V$. Then $d: V \rightarrow \mathbb{N}$ is the \emph{degree function} defined by $d(v) = n \in \mathbb{N}$ if $v$ is incident to $n$ edges in $G$, where loops contribute 2. We also have $N_G: V \rightarrow \mathcal{P}(V)$ being the \emph{neighbourhood function}, where $N_G(v) = \{u \in V:\ uv \in E\}$.\end{defn}
\begin{defn}For a graph $G$, we define the \emph{order} and \emph{size} of G to be $v(G) = |V(G)|$ and $e(G) = |E(G)|$ respectively. \end{defn}
\begin{defn}A \emph{simple graph} is a graph in which there are no loops and no ``parallel edges'' (two edges are \emph{parallel} if they share both endpoints).\end{defn}

\section{Simple Results}
We are now ready to prove some results that follow immediately from our current set of definitions.
\begin{thm}[Degree-Sum Formula]
Let $G = (V,E)$ be a graph with $V = \{v_1,v_2,...,v_n\}$. Then $\sum_{i=1}^{n}d(v_i) = 2e(G)$.
\end{thm}
\begin{proof}
Begin by noting that each edge in $G$ has 2 endpoints. Then it follows that summing over the degree of each vertex in $G$ counts each edge twice. Hence, $\sum_{i=1}^{n}d(v_i) = 2e(G)$.
\end{proof}

\begin{cor}
The sum of the degrees of all vertices in a graph is even.
\end{cor}
\begin{proof}
Let $G$ be a graph. Then, by the Degree-Sum formula, $$\sum_{v \in V(G)}d(v) = 2e(G).$$ Now $e(G) \in \mathbb{N}$ since $E(G)$ is countable, so $\sum_{v \in V(G)}d(v)$ is even by the definition of an even number. 
\end{proof}

\begin{thm}
Let $G$ be a graph. Then there is an even number of vertices of odd degree in $G$.
\end{thm}
\begin{proof}
We have already shown that the sum of the degrees of all vertices in $G$ must be even. Let $D_{odd}$ be the set of vertices in $G$ with odd degree and $D_{even}$ be the set of vertices in $G$ with even degree. Then define $S_{odd} := \sum_{v \in D_{odd}}d(v)$ and $S_{even} := \sum_{v \in D_{even}}d(v)$. Then, since $(D_{odd},D_{even})$ is a partition of $V(G)$, i.e. $D_{odd} \cup D_{even} = V(G)$ and $D_{odd} \cap D_{even} = \varnothing$, $$\sum_{v \in V(G)}d(v) = 2e(G) = S_{odd} + S_{even}.$$ From this we see that either both $S_{odd}$ and $S_{even}$ are odd or they are both even. Since $S_{even}$ is a summation of even numbers, i.e. for all $v \in D_{even}$, $d(v)$ is even, $S_{even}$ must be even, from which it follows that $S_{odd}$ is necessarily also even. Since $S_{odd}$ is a summation of odd numbers in $D_{odd}$, $|D_{odd}|$ must be even, otherwise such a sum would be odd. By our definition of $D_{odd}$, we conclude that the number of vertices in $G$ with odd degree is even.
\end{proof}

\begin{thm}
Let $G$ be a simple graph. Then $e(G) \leq \binom{v(G)}{2}$. \label{bound_edges}
\end{thm}
\begin{proof}
Since $G$ is simple, each edge is a unique unordered pair. Hence, the number of edges in $G$ is bound by the number of unordered pairs of vertices in $G$ (given by $\binom{v(G)}{2}$). That is, $e(G) \leq \binom{v(G)}{2}$.
\end{proof}


\section{Special Classes of Graph}
We are ready to define some simple classes of graph. These classes are fundamental to understanding certain problems and larger graphs can be decomposed into such classes.
\begin{defn}[Directed Graph] A \emph{directed graph} $G=(V,E)$ is a graph with vertex set $V = \{v_1,...,v_n\}$ and edge set $E \subseteq V \times V$, where the ordered pair $(u,v) \in E$ denotes a \emph{directed edge} from $u$ to $v$.\end{defn}
\begin{rem}We can encode the meaning of an undirected graph using a directed graph by using two directed edges in place of one undirected edge.
e.g. For a directed graph $G_D = (V(G_D),E(G_D))$ and undirected graph $G_U = (V(G_U),E(G_U))$, where $V(G_D) = V(G_U)$, we make $G_D$ "equivalent" to $G_U$ by the following rule - whenever we have $\{u,v\} \in E(G_U)$, we put $(u,v)$ and $(v,u)$ into $E(G_D)$. This allows us to travel in both directions between $u$ and $v$ in $G_D$ just as we can in $G_U$, encoding the undirected edge $\{u,v\}$ in $G_U$ by the directed edges $(u,v)$ and $(v,u)$ in $G_D$. \end{rem}
\begin{defn}[Bipartite Graph] A \emph{bipartite graph} $G[X,Y] = (V,E)$ is a simple graph with a bipartition $(X,Y)$ of $V$, i.e. $X \cup Y = V$ and $X \cap Y = \varnothing$, such that all edges have one endpoint in $X$ and one endpoint in $Y$. That is, no edge has both endpoints in $X$ or both endpoints in $Y$.\end{defn}
\begin{defn}[Complete Graph] A \emph{complete graph} $G = (V,E)$ is a simple graph in which, given any vertex $u \in V$, $u$ is adjacent to all $v \in V$. That is, $E = \binom{V}{2}$ in a simple graph. We denote by $K_n$, the complete graph of order $n$, and by $K_{n,m}$, the bipartite graph $G[X,Y]$ with $|X| = n$ and $|Y| = m$, such that all vertices in $X$ are adjacent to all vertices in $Y$. \end{defn}
\begin{defn}[Regular Graph] A graph $G$ is \emph{regular} if for all $v_1,v_2,...,v_n \in V(G)$, \linebreak $d(v_1) = d(v_2) = ... = d(v_n)$. If $d(v_1) = ... = d(v_n) = k$ for some $k \in \mathbb{N}$, then $G$ is said to be \emph{$k$-regular}.\end{defn}

\begin{defn}For a graph $G$, we have the following definitions:
\begin{itemize}
\item A \emph{walk} (in $G$) is a sequence $w = (v_0,e_1,v_1,e_2,...,e_n,v_n)$ of alternating vertices and edges, where $v_i \in V(G)$ and $e_i \in E(G)$ for all $i \leq n$, and $e_j$ joins $v_{j-1}$ and $v_j$ for all $1 \leq j \leq n$. The length of $w$ is the number of edges in $w$.
\item A \emph{path} (in $G$) $p = (v_0,e_1,...,e_n,v_n)$ is a walk (in $G$) where all vertices in $p$ are distinct, except possibly $v_0$ and $v_n$.
\item A \emph{closed walk} (in $G$) $w = (v_0,...,v_n)$ is a walk (in $G$) such that $v_0 = v_n$.
\item A \emph{cycle} (in $G$) $c = (v_0,...,v_n)$ is a closed path (in $G$) with length at least 1.
\end{itemize}
\end{defn}
\begin{note}If $G$ is a simple graph, then we may denote a walk $w$, of length $n$, informally \linebreak by $w = v_0,v_1,...,v_n$. \end{note}
\begin{defn}[Cycle graph] A \emph{cycle graph} $G$ is a graph consisting of a single cycle. We denote by $C_n$, the cycle graph of order $n$.\end{defn}

Going forward in this report, for simplicity, we will use the term "graph" to mean a "simple, undirected graph," unless stated otherwise. We now prove, as before, some useful results following from these definitions.

\begin{lem} Let $G[X,Y]$ be a bipartite graph. Then
\begin{equation*}
\sum_{x \in X}d(x) = \sum_{y \in Y}d(y).
\end{equation*}
\end{lem}
\begin{proof}
Since $G$ is bipartite, each edge in $E(G)$ has one endpoint in $X$ and one in $Y$. Then summing over the the degrees of the vertices in $X$ counts all edges in $E(G)$, and similarly for summing over the vertices in $Y$. That is, $\sum_{x \in X}d(x) = e(G) = \sum_{y \in Y}d(y)$.
\end{proof}
\begin{cor}
Let $G[X,Y]$ be a $k$-regular bipartite graph with $k > 0$. Then $|X| = |Y|$.
\end{cor}
\begin{proof}
By the previous lemma, $\sum_{x \in X}d(x) = \sum_{y \in Y}d(y)$, hence $k \cdot |X| = k \cdot |Y|$, implying $|X| = |Y|$ after division by $k>0$.
\end{proof}

\begin{lem}
Let $G$ be a graph consisting of only a path. Then $G$ is bipartite.
\end{lem}
\begin{proof}
Since $G$ is a path, we can list the vertices in $V(G)$ in a sequence $(v_1,...,v_n)$, such that $v_i$ is adjacent to only $v_{i-1}$ and $v_{i+1}$ for $1<i<n$. So, we have that for all $v_i,v_j \in V(G)$, $v_i$ is adjacent to $v_j$ if and only if $i$ is odd and $j$ is even, or $i$ is even and $j$ is odd. From this, it follows that we have the bipartition $(X,Y)$ of $V(G)$, where $X \subseteq V(G)$ is the set of vertices of odd enumeration and $Y \subset V(G)$ is the set of vertices of even numeration.
\end{proof}

\begin{thm}
Let $K_n$ be the complete graph of order $n$. Then $e(K_n) = \frac{n(n-1)}{2}$. \label{complete_edges}
\end{thm}
\begin{proof}
In $K_n$, we must place an edge between every pair of vertices. Since the pairs are not ordered, we have that $E(K_n) = \binom{V(G)}{2}$, so $e(K_n) = \binom{v(G)}{2} = \binom{n}{2} = \frac{n(n-1)}{2}$.
\end{proof}
\begin{rem}
Theorem \ref{complete_edges} is a special case of Theorem \ref{bound_edges}, namely the case in which equality holds. \\
\end{rem}
We have now proven and reinforced a good basis of knowledge on which we can build intuition about combinatorial games on graphs and the algorithms and theoretical results surrounding them. In the following chapter, we will describe the rules of the game Cops and Robbers in full and mathematically formulate said rules using our definitions given in this chapter. We will relate the graph classes we have studied to parameters within the game which we will describe later.