\chapter{Tree Width}
\label{chapter3}

In this chapter we will present the results of Seymour and Thomas concerning the ``tree width'' property of a graph and its relation to the cop number of said graph \cite{min-max}.

\section{Introduction}
We first describe the concept of monotonicity and monotonely searching a graph.

\begin{defn}
Given a graph $G$, $k \in \mathbb{N}$ cops can \emph{monotonely search} $G$ if and only if the cop player can win in $\mathcal{CR}(G,k)$ with a sequence of moves $X_0,X_1,...,X_n$ such that $X_i \cap X_{i''} \subseteq X_{i'}$ for $0\leq i \leq i' \leq i'' \leq n$. Denote by $c_M(G)$ the minimum number of cops such that they have a monotone winning strategy in $G$.
\end{defn}

\begin{rem}
More intuitively, this is equivalent to the statement ``$k$ cops can search $G$ monotonely if and only if they can search $G$ without returning to any vertices.'' It appears that this property of searching is in some sense ``stricter'' than searching non-monotonely, but they are in fact equivalent. We will later see a theorem that shows this.\\
\end{rem}

\noindent Now we give definitions to describe the tree width of a graph.

\begin{defn}
Given a graph $G$, a \emph{tree-decomposition} of $G$ is a pair $(T,W)$ where $T$ is a tree and $W = \{W_t : t \in V(T)\}$ is a family of subsets of $V(G)$ such that:
\begin{itemize}
\item $\bigcup_{t \in V(T)} W_t = V(G)$;
\item for all $\{u,v\} \in E(G)$, $u,v \in W_t$ for some $W_t$;
\item if $t,t',t'' \in V(T)$ and $t'$ lies on the path from $t$ to $t''$, then $W_t \cap W_{t''} \subseteq W_{t'}$. $(\ast)$
\end{itemize}
The third condition can be restated as:
\begin{itemize}
\item for all $v \in V(G)$, the set of vertices $\{t \in V(T): v \in W_t\}$ induces a connected subgraph of $T$. $(\ast\ast)$
\end{itemize}
\label{treedec_defn}
\end{defn}

\noindent We prove the equivalence of $(\ast)$ and $(\ast\ast)$ below.
\begin{proof}
Let $G$ be a graph and $(T,W)$ a pair where $T$ is a tree and $W = \{W_t: t \in V(T)\}$ a family of subsets of $V(G)$. Assume $(\ast)$ holds for $G$ and $(T,W)$. Take a vertex $x \in V(G)$ and consider the set $A = \{t \in V(T): x \in W_t\}$. We must show that there is a path between any two vertices in $A$. If $|A| = 1$, $A$ induces a connected subgraph of $T$. Otherwise, take arbitrary and distinct $u,v \in A$. A path from $u$ to $v$ exists in $T$, since $T$ is connected, so for any $t \in V(T)$, $W_u \cap W_v \subseteq W_t$. Since $x \in W_u$ and $x \in W_v$, $x \in W_t$ for all $t \in V(T)$ in the path from $u$ to $v$. Hence each vertex on that path is a member of $A$, so any $u,v \in A$ are connected by vertices in $A$. Thus, $(\ast\ast)$ is true. Now assume $(\ast\ast)$ holds for $G$ and $(T,W)$. Choose arbitrary $t,t',t'' \in V(T)$ where $t'$ lies on the path between $t$ and $t''$. For all vertices $v \in V(G)$ such that $v \in W_t \cap W_{t''}$ we have that $t,t'' \in \{x \in V(T): v \in W_x\}$, which induces a connected subgraph of $T$ by $(\ast\ast)$. Hence, $v \in W_u$ for all $u \in V(T)$ on the path between $t$ and $t''$, specifically $v \in W_{t'}$. So we have, for all $v \in W_t \cap W_{t''}$, $v \in W_{t'}$, so $W_t \cap W_{t''} \subseteq W_{t'}$. Thus we have $(\ast)$ if and only if $(\ast\ast)$.
\end{proof}

\begin{defn}
Let $G$ be a graph and $(T,W)$ a tree-decomposition of $G$. Then the \emph{width} of $(T,W)$, denoted $w(T,W)$, is given by $w(T,W) = \max\{|W_t| - 1: t \in V(T)\}$.
\end{defn}

\begin{defn}
The \emph{tree-width} of a graph $G$, $\mathcal{W}(G)$, is the minimum width of a tree-decomposition of $G$. That is, $\mathcal{W}(G) = \min\{w(T,W): (T,W) \text{ is a tree-decomposition of } G\}$. \label{tw_def}
\end{defn}

\begin{ex}
Shown below is an example tree-decomposition with the original graph on the left and the decomposition on the right. This tree-decomposition has width 2 \cite{wiki-treedec}.
%Draw graphs
\begin{center}
\begin{tikzpicture}
%Vertices
\draw (0,4) node[circle,draw](A) {A};
\draw (2,4) node[circle,draw](B) {B};
\draw (0,2) node[circle,draw](C) {C};
\draw (0,0) node[circle,draw](D) {D};
\draw (2,0) node[circle,draw](E) {E};
\draw (4,4) node[circle,draw](F) {F};
\draw (4,2) node[circle,draw](G) {G};
\draw (4,0) node[circle,draw](H) {H};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (B) -- (E);
\draw (B) -- (F);
\draw (B) -- (G);
\draw (C) -- (D);
\draw (C) -- (E);
\draw (D) -- (E);
\draw (E) -- (G);
\draw (E) -- (H);
\draw (F) -- (G);
\draw (G) -- (H);
\end{tikzpicture}
\hspace{1cm}
\begin{tikzpicture}
%Vertices
\draw (0,3) node[circle,draw, scale=0.9](ABC) {A,B,C};
\draw (5,3) node[circle,draw, scale=0.9](BFG) {B,F,G};
\draw (1.5,1.5) node[circle,draw, scale=0.9](BCE) {B,C,E};
\draw (3.5,1.5) node[circle,draw, scale=0.9](BEG) {B,E,G};
\draw (0,0) node[circle,draw, scale=0.9](CDE) {C,D,E};
\draw (5,0) node[circle,draw, scale=0.9](EGH) {E,G,H};

%Edges
\draw (ABC) -- (BCE);
\draw (BCE) -- (CDE);
\draw (BCE) -- (BEG);
\draw (BEG) -- (BFG);
\draw (BEG) -- (EGH);
\end{tikzpicture}
\end{center}
\label{treedec_ex}
\end{ex}

\section{Effects on Cops and Robbers}
Now that we have fully described the tree-width property, we can discuss the implications of this on the game we are studying. The following is an important result by Seymour and Thomas.

\begin{thm}
Let $G$ be a graph and $k \in \mathbb{N}$. Then the following are equivalent:
\begin{enumerate}
\item[$(i)$] $c(G) = k$;
\item[$(ii)$] $c_M(G) = k$;
\item[$(iii)$] $\mathcal{W}(G) = k-1$.
\end{enumerate}
\label{copnum_treewidth}
\end{thm}
\begin{proof}
See \cite{min-max}.
\end{proof}
LaPaugh showed that $(i)$ if and only if $(ii)$ in a game where the robber is invisible \cite{lapaugh}. Thus, this is true for our game in which the robber is visible, since the cops may employ the same search strategy as if the robber were invisible. We will see why $(ii)$ implies $(iii)$ in the next section when we introduce cliques. The proof of $(iii)$ implies $(i)$ is much longer, involving ``screens,'' ``havens,'' and ``jump-searching.'' The idea is to show that, for any ``screen'' $S$ of a graph $G$ and $k \in \nn \setminus \{0\}$, there is no $S' \supseteq S$, where for all $X \in [V(G)]^{<k}$ and $H \in S'$, $X \cap H \neq \varnothing$, if and only if $G$ has a tree-decomposition $(T,W)$ where every $t \in V(T)$ with $|W_t| \geq k$ both has ``valency'' $1$ and $W_t \cap H = \varnothing$ for any $H \in S$. It is then possible to set $S = \varnothing$ in this result to obtain the proof of $(iii)$ implies $(i)$. This is the main focus of Seymour and Thomas \cite{min-max} and involves multiple intermediate steps to attain this result. We refer to their paper for details of the proof and the terms we neglected to define, though we will later meet some of them when discussing strategies for the robber player.

From this theorem, we have established the link between the cop number and tree-width of a graph. We can use this link to answer our earlier question concerning subgraphs:
\begin{center}
\question{Let $G$ and $H$ be graphs with $G$ a subgraph of $H$.}{Is $c(G) \leq c(H)$?}
\end{center}
We confirm that the answer to this question is positive in the following theorem.

\begin{thm}
Let $H$ be a graph and $G$ a subgraph of $H$. Then $c(G) \leq c(H)$. \label{cn_subgraph}
\end{thm}
\begin{proof}
If $V(H) = \varnothing$, then $V(G) = \varnothing$, so $c(H) = c(G) = 0$. If $V(G) = \varnothing$, \linebreak $c(G) = 0 \leq c(H)$. Otherwise, let $(T,W)$ be a tree-decomposition of $H$ such that \linebreak $w(T,W) = \mathcal{W}(H)$. Then we may construct the set $W' = \{W'_t \subseteq V(G): t \in V(T)\}$ where, for each $t \in V(T)$, $W'_t = W_t \cap V(G)$. We have $|W'_t| = |W_t \cap V(G)| \leq |W_t|$ for all $t \in V(T)$, so $w(T,W') \leq w(T,W)$. It remains to show that $(T,W')$ is in fact a tree-decomposition of $G$.
\begin{itemize}
\item We have $\bigcup_{t \in V(T)} W_t = V(H) \supseteq V(G)$. Hence, for all $v \in V(G)$, $v \in W_t$ for some $W_t \in W$, so $v \in (W_t \cap V(G)) = W'_t \in W'$. So each $v \in V(G)$ is a member of some $W'_t \in W'$, i.e. $\bigcup_{t \in V(T)} W'_t = V(G)$.
\item Let $e = \{u,v\} \in E(G)$ where $u,v \in V(G)$. Then $e \in E(H)$ since $E(G) \subseteq E(H)$, and \linebreak $u,v \in V(H)$. So $u,v \in W_t$ for some $W_t \in W$ where $t \in V(T)$. Then $u,v \in (W_t \cap V(G))$, where we have $(W_t \cap V(G)) = W'_t$. Hence, for any edge $\{u,v\} \in E(G)$, $u,v \in W'_t$ for some $W'_t \in W'$.
\item We already have that for all $v \in V(H)$, $\{t \in V(T): v \in W_t\}$ induces a connected subgraph of $T$. Hence this property holds for all $v \in V(G)$ since $V(G) \subseteq V(H)$, so $\{t \in V(T): v \in W_t\}$ induces a subgraph of $T$. Since, for any $v \in V(G)$, $v \in W_t$ for some $t \in V(T)$ only if $v \in W'_t$, it follows that $\{t \in V(T): v \in W'_t\}$ induces a subgraph of $T$.
\end{itemize}
Hence, comparing with Definition \ref{treedec_defn}, $(T,W')$ is a tree-decomposition of $G$ and we have $w(T,W') \leq w(T,W)$. So
\begin{equation*}
c(H) = \mathcal{W}(H) + 1 = w(T,W) + 1 \geq w(T,W') + 1 \geq \mathcal{W}(G) + 1 = c(G).
\end{equation*}
\end{proof}

An intuitive outline of the previous proof is that we take the minimum width tree-decomposition of the supergraph and remove the vertices not in the subgraph from the sets associated with the vertices of the tree in the decomposition. From this we obtain a new tree-decomposition of the subgraph with a smaller width.

\begin{cor}
Let $H$ be a graph and $G$ a subgraph of $H$. Then $\mathcal{W}(G) \leq \mathcal{W}(H)$.
\end{cor}
\begin{proof}
The result follows immediately from Theorem \ref{copnum_treewidth} and Theorem \ref{cn_subgraph}.
\end{proof}

Using Theorem \ref{copnum_treewidth} and Theorem \ref{cn_subgraph}, we can characterise the properties of graphs with a given cop number.

\begin{cor}
Let $G$ be a graph. Then $c(G) = 1$ if and only if $e(G) = 0$. \label{cn=1}
\end{cor}
\begin{proof}
We will show that $\mathcal{W}(G) = 0$ if and only if the size of $G$ is $0$. Suppose \linebreak $\mathcal{W}(G) = 0 = w(T,W)$ for a tree decomposition $(T,W)$ of $G$ with minimum width. Then for any $W_t \in W$, $|W_t| = 1$ and hence no $W_t$ contains the endpoints of an edge. Since both endpoints of each edge of $G$ are in some $W_t \in W$, $G$ has no edges, so $e(G) = 0$. Now suppose $e(G) = 0$, so $G$ has no edges. Hence, taking an arbitrary tree $T$ such that $v(T) = v(G)$, we may construct a tree-decomposition $(T,W)$ of minimum width by assigning each vertex in $V(G)$ to a different set $W_t \in W$. Thus, we have $w(T,W) = \max\{|W_t| - 1: t \in V(T)\} = 0$. Hence, $\mathcal{W}(G) = 0$, so $c(G) = 1$. It now follows that $c(G) = 1$ if and only if $\mathcal{W}(G) = 0$ if and only if $e(G) = 0$.
\end{proof}

\begin{cor}
Let $G$ be a graph. Then $c(G) = 2$ if and only if $G$ is a forest with $e(G) > 0$.
\end{cor}
\begin{proof}
We prove the ``if'' direction first. A graph $T$ is a tree on two or more vertices (i.e. $e(T) > 0$) only if $\mathcal{W}(T) = 1$ \cite{die05}, so $c(T) = 2$. Hence if $G$ is a forest with $e(G) > 0$, then $c(G) \geq 2$ by Theorem \ref{cn_subgraph}. Indeed $c(G) \leq 2$ since the robber must select a tree of $G$ and 2 cops can catch the robber within this tree. Thus we have $c(G) = 2$. Conversely, assume $c(G) = 2$ and that $G$ is not a forest with $e(G) > 0$. If $e(G) = 0$ then $c(G) = 1$ by Corollary \ref{cn=1}. Otherwise, $G$ contains a cycle since it is not a forest, so $C_n$ is a subgraph of $G$ for some natural number $n \leq v(G)$. $c(C_n) = 3$ by Theorem \ref{cn_cycle}, so $c(G) \geq 3$ by Theorem \ref{cn_subgraph}. In either case we have a contradiction, so $G$ is a forest with $e(G) > 0$.
\end{proof}

These two corollaries, following from the powerful Theorems \ref{copnum_treewidth} and \ref{cn_subgraph}, have allowed us to completely characterise the graphs with cop number two or less. In general, determining the tree-width - and therefore the cop number - of an arbitrary graph is an $\mathbb{NP}$-complete problem. The next chapter will discuss the meaning of this classification of the problem and algorithms designed to solve it.

\section{An Equivalent Definition}
In this section, we will state a more intuitive description of the tree-width property that will make Theorem \ref{copnum_treewidth} clearer.

\begin{defn}
Given a graph $G$, a \emph{clique} $C \subseteq V(G)$ of $G$ is a set of vertices such that all vertices in $C$ are adjacent in $G$. Equivalently, the subgraph of $G$ induced by $C$ is complete. The \emph{clique number} of $G$, $\omega(G)$, is the size of the largest clique in $G$. \label{clique_defn}
\end{defn}
\begin{defn}
Given a cycle $c$, a \emph{chord} of $c$ is an edge that joins two vertices of $c$, but is not in $c$ itself.
\end{defn}
\begin{defn}
Let $G$ be a graph. Then $G$ is \emph{chordal} if every cycle in $G$, of length greater than 3, has at least one chord. \label{chordal-def}
\end{defn}

From Definition \ref{treedec_defn}, Example \ref{treedec_ex}, and Definition \ref{clique_defn}, it should be clear that a tree-decomposition of a graph in some sense separates that graph into cliques, represented by the tree structure of the decomposition. In fact, the tree-width of a graph is definable in this way, as we shall see.

\begin{defn}
Let $S$ and $I$ be sets and $\{S_i \subseteq S: i \in I\}$ a family of subsets of $S$. Then $S$ satisfies the \emph{Helly property} if, for all subsets $J \subseteq I$ such that for all $j,k \in J$, $S_j \cap S_k \neq \varnothing$, we have that $\bigcap_{j \in J} S_j \neq \varnothing$ holds.
\end{defn}

\begin{thm}
Let $T$ be a tree and $I$ a set with $J = \{T_i \subseteq V(T): i \in I\}$ a family of subsets of vertices of $T$, inducing subtrees. Then $J$ has the Helly property. \label{helly-prop-trees}
\end{thm}
\begin{proof}
See \cite{golumbic}.
\end{proof}

\begin{lem}[Clique containment lemma \cite{cographs}]
Let $G$ be a graph, $(T,W)$ a tree-decomposition of $G$, and $C \subseteq V(G)$ a clique in $G$. Then for some $W_t \in W$, $C \subseteq W_t$. \label{clique-cont}
\end{lem}
\begin{proof}
Let $T_v = \{t \in T: v \in W_t\}$, so $\{T_v: v \in C\}$ is a family of induced subtrees of $T$. By Theorem \ref{helly-prop-trees}, $\{T_v: v \in C\}$ has the Helly property. Hence, we have for all $u,v \in C$, $u,v \in W_t$ for some $t \in V(T)$ since $uv \in E(G)$, so $t \in T_u \cap T_v$, implying $\bigcap_{v \in C} T_v \neq \varnothing$. That is, there exists some $t \in V(T)$ such that for all $v \in C$, $v \in W_t$.
\end{proof}

\begin{lem}
Given a graph $G$, $\mathcal{W}(G) \geq \omega(G) - 1$. \label{tw-clique}
\end{lem}
\begin{proof}
Let $(T,W)$ be a tree-decomposition of G of minimum width. By Lemma \ref{clique-cont}, there is some $t \in V(T)$ with $|W_t| = \omega(G)$ since each clique of $G$ is assigned to a vertex of $T$. That is $w(T,W) \geq |W_t| - 1 = \omega(G) - 1$. Hence we have $\mathcal{W}(G) = w(T,W) \geq \omega(G) - 1$.
\end{proof}

We now show that, for chordal graphs, equality holds in the previous lemma. We may then use this result to complete this section by relating tree-width, clique number, subgraphs, and chordal graphs in our equivalent formulation of tree-width. To do this we need the following lemma.

\begin{lem}
A graph $G$ is chordal if and only if $G$ has a tree-decomposition $(T,W)$ such that, for all $W_t \in W$, $W_t$ is a clique in $G$. \label{diestel_chordal}
\end{lem}
\begin{proof}
See \cite{die05}.
\end{proof}

\begin{thm}
Let $G$ be a chordal graph. Then $\mathcal{W}(G) = \omega(G) - 1$. \label{chordal-tw-cliquenum}
\end{thm}
\begin{proof}
By Lemma \ref{tw-clique} we have $\mathcal{W}(G) \geq \omega(G) - 1$. By Lemma \ref{diestel_chordal}, $G$ has a tree-decomposition $(T,W)$ such that every $W_t \in W$ is a clique of $G$. Hence, for all $W_t \in W$, $|W_t| \leq \omega(G)$. Thus, $\mathcal{W}(G) \leq w(T,W) \leq \omega(G) - 1$, so we have $\mathcal{W}(G) = \omega(G) - 1$.
\end{proof}

\begin{thm}
Let $G$ be a graph and define a chordal graph $H$, with the smallest $\omega(H)$, such that $G$ is a subgraph of $H$. Then $\mathcal{W}(G) = \omega(H) - 1$.\label{tw-clique-chordal-supergraph}
\end{thm}
\begin{proof}
Let $A$ be the set of chordal supergraphs of $G$. Then we seek to prove that \linebreak $\mathcal{W}(G) = \min\{\omega(H) - 1: H \in A\}$. Each graph $H \in A$ has a tree-decomposition $(T,W)_H$ such that $w(T,W)_H = \omega(H) - 1$ since $H$ is chordal. Hence, since $G$ is a subgraph of each $H \in A$, we have $\mathcal{W}(G) \leq \mathcal{W}(H) \leq w(T,W)_H = \omega(H) - 1$. Now we construct a chordal graph $H$ such that $\mathcal{W}(G) \geq \omega(H) - 1$. For a tree-decomposition $(T,W)$ of $G$ such that $w(T,W) = \mathcal{W}(G)$, we let $V(H) = V(G)$ and $E(H) = \bigcup_{t \in V(T)} \binom{W_t}{2}$. $(T,W)$ is a tree-decomposition of $H$ since: $\bigcup_{t \in V(T)} W_t = V(H)$; for all $uv \in E(H)$, $u,v \in W_t$ for some $t \in V(T)$; for all $v \in V(H)$, $\{t \in V(T): v \in W_t\}$ is a connected subgraph of $T$, following from $(T,W)$ being a tree-decomposition of $G$. Hence, $H$ is chordal (by Lemma \ref{diestel_chordal}) since $(T,W)$ is a tree-decomposition of cliques of $H$, so $\mathcal{W}(G) = w(T,W) \geq \omega(H) - 1$. Thus $\mathcal{W}(G) = \omega(H) - 1$. \\
\end{proof}

The intuition for this formulation of tree-width is that the width of any tree-decomposition of a graph must be at least the size of the largest clique in that graph. Conveniently, we may bound this above by the clique number of chordal supergraphs to attain our resulting equality. For additional clarity and completeness, we present the following proposition.

\begin{prop}
Let $G$ and $H$ be graphs with $G$ a subgraph of $H$. Then $\omega(G) \leq \omega(H)$. \label{clique_subgraph}
\end{prop}
\begin{proof}
Let $C \subseteq V(G)$ be a clique in $G$ such that $\omega(G) = |C|$. Since $G$ is a subgraph of $H$, for any edge $e \in E(G)$, $e \in E(H)$. Similarly, for any vertex $v \in V(G)$, $v \in V(H)$. Thus, it follows that $C$ is also a clique of $H$. Hence we have $\omega(G) = |C| \leq \omega(H)$.
\end{proof}

Establishing that the clique number doesn't increase when taking subgraphs should now clarify the fact that cop number also does not increase, since if the robber player wishes to play optimally, he will exploit the cliques of a graph to allow the most ``freedom of movement'' in some sense. We finalise this section by returning to the graph shown in Example \ref{treedec_ex} and demonstrating the use of the results we have presented in this chapter.

\begin{ex}
We saw previously that the below graph has a tree-decomposition of width 2. Thus, by Definition \ref{tw_def}, the tree-width of this graph is $\leq 2$. Hence, by Theorem \ref{copnum_treewidth}, its cop number is $\leq 3$.

%Draw graph
\begin{center}
\begin{tikzpicture}
%Vertices
\draw (0,4) node[circle,draw](A) {A};
\draw (2,4) node[circle,draw](B) {B};
\draw (0,2) node[circle,draw](C) {C};
\draw (0,0) node[circle,draw](D) {D};
\draw (2,0) node[circle,draw](E) {E};
\draw (4,4) node[circle,draw](F) {F};
\draw (4,2) node[circle,draw](G) {G};
\draw (4,0) node[circle,draw](H) {H};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (B) -- (E);
\draw (B) -- (F);
\draw (B) -- (G);
\draw (C) -- (D);
\draw (C) -- (E);
\draw (D) -- (E);
\draw (E) -- (G);
\draw (E) -- (H);
\draw (F) -- (G);
\draw (G) -- (H);
\end{tikzpicture}
\end{center}

We observe that this graph contains multiple instances of $K_3$ as subgraphs. We saw from Theorem \ref{cn_complete} that $c(K_3) = 3$, and so, by Theorem \ref{cn_subgraph}, the cop number of this graph is $\geq 3$. So we deduce that the minimum number of cops that may win on this graph is 3.
\end{ex}

From this example, we have demonstrated the usefulness of tree-decompositions and subgraphs to bound the cop number of a given graph. While subgraphs can be readily observed from diagrams, it is not so obvious how we may obtain tree-decompositions with the required width in order to sufficiently bound the cop number of a graph. This will be the subject of the following chapter.