\chapter{Strategies for Robbers}
\label{chapter5}

We have previously discussed strategies for cops and their relation to tree-decompositions. We now wish to outline a strategy for the robber player to evade capture. We said briefly that a robber player should utilise the cliques of a graph to effectively elude cops, and in this chapter we shall clarify this statement.

\section{Havens and Screens}
To describe evasion strategies for robbers we must define a new type of function. The robber can use this function to tell him where to move to avoid capture.

\begin{defn}
Given an graph $G$, two subsets $X,Y \subseteq V(G)$ \emph{touch} if either $X \cap Y \neq \varnothing$ or there exist vertices $u \in X$ and $v \in Y$ such that $uv \in E(G)$.
\end{defn}

\begin{defn}
Let $G$ be a graph. A \emph{haven} of order $k \in \nn$ in $G$ is a function $\beta$ that associates each subset $X \in [V(G)]^{<k}$ with an $X$-flap $\beta(X)$, such that $\beta(X)$ touches $\beta(Y)$ for all $X,Y \in [V(G)]^{<k}$
\end{defn}

We may also describe a robber's strategy in terms of touching sets of vertices.

\begin{defn}
Given a graph $G$, a \emph{screen} $S \subseteq \mathcal{P}(V(G))$ in $G$ is a set of subsets of connected vertices in $G$ such that, for any two sets $A,B \in S$, $A$ and $B$ touch. We say $S$ has \emph{thickness} at least $k \in \nn$ ($\tau(S) \geq k$) if, for all $X \in [V(G)]^{<k}$ there is some $H \in S$, $X \cap H = \varnothing$.
\end{defn}

\begin{ex}
Here is an example of a screen in a graph. Each differently coloured set of vertices is a different set in the screen.

%Configure coloured shapes
\pgfdeclarelayer{bg}
\pgfsetlayers{bg,main}
\tikzset{wrap/.style={line cap=round,#1,line width=21pt,opacity=0.3,rounded corners}}

%Draw graph
\begin{center}
\begin{tikzpicture}

%Vertices
\draw (0,0) node[circle,draw](A) {};
\draw (0,2) node[circle,draw](B) {};
\draw (2,0) node[circle,draw](C) {};
\draw (2,2) node[circle,draw](D) {};
\draw (4,1) node[circle,draw](E) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (B) -- (D);
\draw (C) -- (D);
\draw (C) -- (E);
\draw (D) -- (E);

%Coloured sets
\begin{pgfonlayer}{bg}
	\draw[wrap=red](B.center)			to(B.center);
	\draw[wrap=yellow](A.center)		to(C.center)		to(D.center);
	\draw[wrap=blue](C.center)			to(E.center);
	\draw[wrap=green](D.center)		to(E.center);
\end{pgfonlayer}

\end{tikzpicture}
\end{center}

\noindent From inspection we see that this screen has thickness $\geq 3$, since we cannot choose a set of $1$ or $2$ vertices that intersects all sets in the screen. We may, however, choose a set of $3$ vertices that intersects all sets in the screen, so this screen must indeed have thickness $3$. Note that this is not necessarily the only screen of thickness 3 in this graph. \label{screen-example}
\end{ex}

The two previous definitions are in fact equivalent, as we prove with the following lemma. Seymour and Thomas used screens in their paper, but many authors prefer to use ``brambles,'' which are also equivalent to screens and havens. They are sets of connected, touching subsets of vertices similar to screens, but their order is defined to be the size of the smallest set of vertices intersecting all sets in the bramble.

\begin{lem}
Let $G$ be a graph. Then $G$ has a screen of thickness at least $k$ if and only if $G$ has a haven of order at least $k$. \label{screen-haven-equiv}
\end{lem}
\begin{proof}
First assume that $G$ has a screen $S$ such that $\tau(S) \geq k \in \nn$. Then, by definition, for all $X \in [V(G)]^{<k}$, there exists $H \in S$ with $X \cap H = \varnothing$. Now let $\beta$ be a function mapping $X \in [V(G)]^{<k}$ to the $X$-flap containing such an $H \in S$. We claim that $\beta$ is a haven of order $k$ and now prove that claim. Take $X,Y \in [V(G)]^{<k}$ with $H_X, H_Y \in S$ such that $X \cap H_X = \varnothing$ and $Y \cap H_Y = \varnothing$, so we have $X$-,$Y$-flaps $\beta(X) \supseteq H_X$ and $\beta(Y) \supseteq H_Y$. Now, since $H_X$ and $H_Y$ touch, if $H_X \cap H_Y \neq \varnothing$, we have $\beta(X) \cap \beta(Y) \neq \varnothing$ and we are done. Otherwise there exist vertices $u \in H_X$ and $v \in H_Y$ with $uv \in E(G)$. Hence, we also have $u \in \beta(X)$ and $v \in \beta(Y)$ with $uv \in E(G)$. So in either case $\beta(X)$ and $\beta(Y)$ touch. Thus $\beta$ is indeed a haven of order $k$. Now assume that $\beta$ is a haven of order at least $k$ in $G$. We claim that the set $S = \{\beta(X): X \in [V(G)]^{<k}\}$ is a screen with $\tau(S) \geq k$ and prove this. Since $\beta$ is a haven of order at least $k$, we deduce that each $\beta(X) \in S$ is an $X$-flap (and is therefore connected by Definition \ref{flaps}) and all sets in $S$ are touching. Now for any subset $X \in [V(G)]^{<k}$ we have that$X \cap \beta(X) = \varnothing$, and hence $S$ is a screen in $G$ with $\tau(S) \geq k$.
\end{proof}

Thus far, the concepts we have established may not seem obviously connected to any form of strategy in Cops and Robbers, but we may extend our earlier theorem to include our new developments.

\begin{thm}[\ref{copnum_treewidth} extended]
Let $G$ be a graph and $k \in \mathbb{N}$. Then the following are equivalent:
\begin{enumerate}
\item[$(i)$] $c(G) = k$;
\item[$(ii)$] $c_M(G) = k$;
\item[$(iii)$] $\mathcal{W}(G) = k-1$;
\item[$(iv)$] $G$ has a screen of thickness $k$;
\item[$(v)$] $G$ has a haven of order $k$.
\end{enumerate}
\label{copnum_treewidth_ext}
\end{thm}

We have already proven $(iv)$ if and only if $(v)$ in Lemma \ref{screen-haven-equiv} and $(iii)$ implies $(iv)$ is the main focus of \cite{min-max}. So we are left to prove $(v)$ implies $(i)$. From this theorem we conclude that a graph with a haven of order $>k$ or screen of thickness $>k$ does not permit a $k$-cop winning strategy, and hence there may be some special property of these concepts that a robber may utilise for success. This statement will become clear when we prove $(v)$ implies $(i)$ in the next section.

\section{Robber Strategies from Havens}
In this section we prove the link between havens and cop number seen in Theorem \ref{copnum_treewidth_ext} and discuss how this gives the robber an evasion strategy. This, however, in a game with infinite moves and a perfect cop player, is the only way for the robber player to not lose. If such a haven or screen does not exist then the robber player is certain to lose, and so it is imperative that he takes advantage of the strategy that we shall demonstrate.

\begin{lem}
Let $G$ be a graph and $k \in \nn$. If $G$ has a haven of order $k$, then $c(G) = k$.
\end{lem}
\begin{proof}
Assume $G$ has a haven $\beta$ of order $k$. We will first show that $<k$ cops cannot win and then show that $k$ cops can win. Consider the game $\mathcal{CR}(G,k-1)$ with a sequence of moves $(\varnothing,R_0), (X_1,R_1),...$ possibly infinite. For any cop move $X_i$, we have that $X_i \in [V(G)]^{<k}$ by definition, and hence we may apply $\beta$ to such a move to obtain an $X_i$-flap. So the robber may choose $R_i = \beta(X_i)$ at any step in the game. He can, of course, do this because the component of the previous move $R_{i-1} = \beta(X_{i-1})$ touches $R_i$ since $\beta$ is a haven. Thus, since each robber move $R_i$ is an $X_i$-flap, the cop player can never achieve the condition $R_{i-1} \subseteq X_i$ and so the cop player can never win. Now consider $\mathcal{CR}(G,k)$. Since $\beta$ is not a haven of order $k+1$ in $G$, there exists some cop move $X_i$ for which no $X_i$-flap touches another $X_j$-flap for some cop move $X_j$. Executing the move $X_i$ allows the cop to corner the robber, since they now cannot move from the $X_i$-flap they have chosen. Hence, the cop player has created a new induced graph $G'$ with vertices equal to the $X_i$-flap in which they may now win.
\end{proof}

With this lemma we complete the proof of Theorem \ref{copnum_treewidth_ext}. The proof of this lemma has also given us the strategy for the robber player: for a graph $G$ with haven $\beta$, choose the move $R_i = \beta(X_i)$ for $i \geq 1$. This will ensure the survival of the robber should the cop player have fewer cops than the order of the haven. We also see that the robber can survive if and only if such a haven exists, so capture is inevitable in $\mathcal{CR}(G,k)$ if no haven of order $>k$ exists. It is easy to see the comparative use of screens in a robber strategy. That is, for any graph $G$ with screen $S$ such that $\tau(S) >k$, the robber can survive in $\mathcal{CR}(G,k)$ by selecting their move $R_i$ to be $R_i = H \in S$ such that $X_i \cap H = \varnothing$ which is guaranteed to exist by the definition of $S$. Hence, the cop may never achieve $X_i \supseteq R_{i-1}$ to win. \\

To conclude this chapter, we will, as discussed earlier, comment on how cliques may be used by the robber player to evade capture. The following propositions will allow us to do this.

\begin{prop}
Let $G$ be a graph with a clique $C$ and $G_C$ the subgraph of $G$ induced by $C$. Then there exists a haven $\beta_C$ of order $|C|$ in $G_C$.
\end{prop}
\begin{proof}
We claim that $\beta_C(X) = V(G_C) \setminus X$ for $X \in [V(G_C)]^{<|C|}$ is a haven of order $|C|$. Take arbitrary $X,Y \in [V(G_C)]^{<|C|}$ and consider the $X$-,$Y$-flaps $\beta_C(X) = V(G_C) \setminus X$ and \linebreak $\beta_C(Y) = V(G_C) \setminus Y$ in $G_C$. If we find that $\beta_C(X) \cap \beta_C(Y) \neq \varnothing$ then we are done. Otherwise, if we find that $\beta_C(X) \cap \beta_C(Y) = \varnothing$, then, since for all distinct $u,v \in V(G_C)$, $uv \in E(G_C)$, we have $uv \in E(G_C)$ for all $u \in \beta_C(X)$ and $v \in \beta_C(Y)$. Hence $\beta_C(X)$ and $\beta_C(Y)$ touch for any $X,Y \in [V(G_C)]^{<|C|}$, so $\beta_C$ is a haven of order $|C|$ in $G_C$.
\end{proof}

\begin{prop}
Let $G$ be a graph with a clique $C$ and $G_C$ the subgraph of $G$ induced by $C$. Then there exists a screen $S_C$ of thickness $|C|$ in $G_C$.
\end{prop}
\begin{proof}
We claim that $S = \{\{v\}: v \in V(G_C)\}$ is a screen of thickness $|C|$ in $G_C$. Each singleton in $S$ is a connected set of vertices and since we have $uv \in E(G_C)$ for all distinct $u,v \in V(G_C)$, all sets in $S$ touch. Now take arbitrary $X \in [V(G_C)]^{<|C|}$. Since $|X| < |C| = |V(G_C)|$, there exists some vertex $v \in V(G_C)$ not in $X$. We have $\{v\} \in S$ by definition, so we have $X \cap \{v\} = \varnothing$. Hence $S$ is a screen in $G_C$ with $\tau(S) = |C|$.
\end{proof}

From these propositions, we determine that, for a graph $G$ with a clique of size $k+1$, the robber can evade capture in $\mathcal{CR}(G,k)$ by using the haven or screen for the clique that we demonstrated in the propositions above. Thus, in a graph with a clique of size larger than the number of available cops, the cop player will never win if playing against a robber who uses such a strategy. We see that this is consistent with Theorem \ref{cn_subgraph} since cliques induce complete graphs, which then force the cop number of the supergraph to be at least the order of the complete subgraph.