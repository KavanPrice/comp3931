\chapter{Cops and Robbers}
\label{chapter2}

\section{An Informal Description}
Cops and Robbers is a combinatorial game played by two adversaries on a graph (simple and undirected). One player controls a set of cops and the other controls a single robber. The aim of the player controlling the cops is to ``catch'' the robber and the robber's aim is to flee capture for as long as possible (possibly indefinitely). At the beginning of the game, the robber may place himself anywhere in the graph, and then players must alternate turns moving between vertices of the graph. Following the robber's turn, the cops may drop, via helicopter, onto any set of vertices in the graph but must first announce such vertices and also allow the robber to move along edges of the graph to a new location before they land. The robber may not travel to a vertex with a cop on it and may only travel along edges of the graph, i.e. the robber cannot travel between different ``components'' of a graph, but does so instantaneously. Only one cop may occupy a single vertex. Multiple cops can be moved in a single turn and must be moved via helicopter. The game ends when a cop lands on the same vertex as the robber, in which case the cops win, or when the cops resign upon realising it is not possible to catch the robber, in which case the robber wins. Both the cops and the robber remain visible to all parties throughout the game.

\section{Preliminary Definitions}
We give some definitions necessary to give our described game a precise formulation.
\begin{defn}
For a (simple, undirected) graph $G = (V,E)$, we have the following:
\begin{itemize}
\item $H$ is a \emph{subgraph} of $G$ if $V(H) \subseteq V(G)$ and $E(H) \subseteq E(G)$, under the constraint that for all $uv \in E(G)$, if $uv \in E(H)$ then $u,v \in V(H)$.
\item For $u,v \in V$, $u$ and $v$ are \emph{connected} if there exists a walk from $u$ to $v$ in $G$. $u$ and $v$ are \emph{disconnected} if there exists no such walk.
\item $G$ is a \emph{connected graph} if for all $u,v \in V$, $u$ and $v$ are connected.
\item $G$ is a \emph{disconnected graph} if there exist $u,v \in V$ such that $u$ and $v$ are disconnected.
\end{itemize}
\end{defn}

\begin{defn}[Bondy, Murty]
Let $G$ be a disconnected graph. Then the \emph{connected components}, or simply \emph{components}, of $G$ are the vertex-disjoint subgraphs of $G$ such that their union is $G$ \cite{bm08}.
\end{defn}

\begin{rem}
This definition has multiple equivalent forms. For example, given a graph $G$, a component of $G$ is a maximally connected subgraph of $G$, i.e. a subgraph in which all vertices are connected and adding a further vertex would disconnect the subgraph. We can define connected components of $G$ with an equivalence relation $\sim$ on $V(G)$, where for $u,v \in V(G)$, $u \sim v$ if and only if $u$ and $v$ are connected. Then the equivalence class, notated $[u]\text{\texttildelow}$, of a vertex $u \in V(G)$ is the vertex set of the connected component that $u$ belongs to, since $[u]\text{\texttildelow} = \{v: u \sim v\} = \{v: \text{$u$ and $v$ are connected} \}$. To form the edges of the component created by $[u]\text{\texttildelow}$, we simply place an edge between $v,w \in [u]\text{\texttildelow}$ if $vw \in E(G)$. So we get all connected components of $G$ by iterating this process on all vertices of $G$.
\end{rem}

\begin{defn}[Seymour, Thomas \cite{min-max}]
Let $G$ be a simple, undirected graph. Then we have the following definitions:
\begin{itemize}
\item $G \setminus X$ is the graph obtained from $G$ by deleting $X$, where $X$ may be a vertex, an edge, or a set of vertices or edges.
\item The vertex set of a component of $G \setminus X$ is called an \emph{$X$-flap}.
\item For a set $A$ and $k \in \mathbb{N}$, we denote by $[A]^{<k}$ the set of all subsets of $A$ of cardinality less than $k$. That is, $[A]^{<k} = \{S \in \mathcal{P}(A): |S| < k\} \subseteq \mathcal{P}(A)$.
\end{itemize} \label{flaps}
\end{defn}

\section{Formal Rules of the Game}
We are now ready to give a mathematical formulation to the rules of the game (attributed to Seymour and Thomas \cite{min-max}). Let $G = (V,E)$ be a simple, undirected graph. Given $k-1 \in \mathbb{N}$ cops, the game is denoted $\mathcal{CR}(G,k-1)$, and a position in $\mathcal{CR}(G,k-1)$ is an ordered pair $(X,R)$ where $X \in [V(G)]^{<k}$ and $R$ is an $X$-flap. $X$ is the set of vertices currently occupied by cops and $R$ is the vertex set of the component of $G \setminus X$ occupied by the robber. Since, as described before, the robber may run arbitrarily fast, it suffices to identify the component they occupy, not a specific vertex of that component, since they may travel instantaneously to any vertex in that component. The game consists of a sequence of positions $(X_0,R_0), (X_1,R_1), ..., (X_n,R_n)$, where on the $i^{th}$ step of the game we begin with $(X_{i-1},R_{i-1})$ and progress to position $(X_i,R_i)$. $(X_0,R_0)$ is the initial position of the game with $X_0 = \varnothing$ and $R_0$ chosen by the robber player. In each subsequent step, the cop player chooses a new set $X_i \in [V(G)]^{<k}$ such that $X_{i-1} \subseteq X_i$ or $X_i \subseteq X_{i-1}$, after which the robber player chooses an $X_i$-flap $R_i$ such that $R_i \subseteq R_{i-1}$ or $R_{i-1} \subseteq R_i$. The cop player wins if $V(R_{i-1}) \subseteq X_i$, that is, the cops cover all vertices of the robber's component. The robber wins if they can manage to avoid capture indefinitely.
\begin{defn}
Let $G$ be a graph. The \emph{cop number} of $G$, $c(G)$, is the minimum number of cops such that there is a winning strategy for the cop player.
\end{defn}

In order to clarify the procedure we have just formulated, we will step through a visual example in the next section. This will give intuition to the game we have created and put us in a good position to begin discussing cop strategies for specific graphs.

\section{An Example Game}
In this section we demonstrate an example pertaining to the Cops and Robbers game we have described. We will first show the graph and then each position on the graph as the game progresses.
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\tikzstyle{arrow} = [ultra thick,->,>=stealth]

\begin{ex}
We define the graph $G$ below and consider the game $\mathcal{CR}(G,3)$.

\begin{figure}[H]
\begin{center}
\begin{tikzpicture}

%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw](D) {};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw](F) {};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw](H) {};
\draw (3,5) node[circle,draw](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

\end{tikzpicture}
\end{center}
\centering
$G$
\end{figure}
We proceed with the game below with blue cops $\{c_1,c_2,c_3\}$. For simplicity, we denote the robber's position with only a single red vertex, though we consider the whole $X_i$-flap containing that red vertex to be his true position for some cop move $X_i$.

\begin{center}
\begin{longtable}{c c}

%Row 1
\multicolumn{2}{c}{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw,fill=red](C) {};
\draw (1,4) node[circle,draw](D) {};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw](F) {};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw](H) {};
\draw (3,5) node[circle,draw](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cops
\draw (-2,6) node[circle,draw,fill=blue,text=white](c_1) {$c_1$};
\draw (-1,6) node[circle,draw,fill=blue,text=white](c_2) {$c_2$};
\draw (-2,5) node[circle,draw,fill=blue,text=white](c_3) {$c_3$};
\end{tikzpicture}
\centering
$R_0$
} \\
\hline
%Row2
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw,fill=red](C) {};
\draw (1,4) node[circle,draw](D) {};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw](F) {};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw](H) {};
\draw (3,5) node[circle,draw](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cops
\draw (-2,6) node[circle,draw,fill=blue,text=white](c_1) {$c_1$};
\draw (-1,6) node[circle,draw,fill=blue,text=white](c_2) {$c_2$};
\draw (-2,5) node[circle,draw,fill=blue,text=white](c_3) {$c_3$};

%Cop arrows
\draw [arrow] (c_1) -- (F); 
\end{tikzpicture}
\centering
$X_1$
}
&
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw](D) {};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw,fill=blue,text=white](F) {$c_1$};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw](H) {};
\draw (3,5) node[circle,draw](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw,fill=red](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cops
\draw (-1,6) node[circle,draw,fill=blue,text=white](c_2) {$c_2$};
\draw (-2,5) node[circle,draw,fill=blue,text=white](c_3) {$c_3$};
\end{tikzpicture}
\centering
$R_1$
} \\
\hline

%Row3
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw](D) {};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw,fill=blue,text=white](F) {$c_1$};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw](H) {};
\draw (3,5) node[circle,draw](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw,fill=red](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cops
\draw (-1,6) node[circle,draw,fill=blue,text=white](c_2) {$c_2$};
\draw (-2,5) node[circle,draw,fill=blue,text=white](c_3) {$c_3$};

%Cop arrows
\draw [arrow] (c_2) -- (H);
\draw [arrow] (c_3) -- (D);
\end{tikzpicture}
\centering
$X_2$
}
&
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw,fill=blue,text=white](D) {$c_3$};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw,fill=blue,text=white](F) {$c_1$};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw,fill=blue,text=white](H) {$c_2$};
\draw (3,5) node[circle,draw,fill=red](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);
\end{tikzpicture}
\centering
$R_2$
} \\
\hline

%Row4
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw,fill=blue,text=white](D) {$c_3$};
\draw (1,5) node[circle,draw](E) {};
\draw (2,3) node[circle,draw,fill=blue,text=white](F) {$c_1$};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw,fill=blue,text=white](H) {$c_2$};
\draw (3,5) node[circle,draw,fill=red](I) {};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cop arrows
\draw [arrow] (F) -- (I);
\end{tikzpicture}
\centering
$X_3$
}
&
{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw,fill=blue,text=white](D) {$c_3$};
\draw (1,5) node[circle,draw,fill=red](E) {};
\draw (2,3) node[circle,draw](F) {};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw,fill=blue,text=white](H) {$c_2$};
\draw (3,5) node[circle,draw,fill=blue,text=white](I) {$c_1$};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);
\end{tikzpicture}
\centering
$R_3$
} \\
\hline

%Row5
\multicolumn{2}{c}{
\begin{tikzpicture}
%Vertices
\draw (0,1) node[circle,draw](A) {};
\draw (1,0) node[circle,draw](B) {};
\draw (1,2) node[circle,draw](C) {};
\draw (1,4) node[circle,draw,fill=blue,text=white](D) {$c_3$};
\draw (1,5) node[circle,draw,fill=red](E) {};
\draw (2,3) node[circle,draw](F) {};
\draw (3,0.5) node[circle,draw](G) {};
\draw (3,4) node[circle,draw,fill=blue,text=white](H) {$c_2$};
\draw (3,5) node[circle,draw,fill=blue,text=white](I) {$c_1$};
\draw (3.5,2) node[circle,draw](J) {};
\draw (4,3) node[circle,draw](K) {};

%Edges
\draw (A) -- (B);
\draw (A) -- (C);
\draw (B) -- (C);
\draw (C) -- (F);
\draw (D) -- (E);
\draw (D) -- (F);
\draw (D) -- (H);
\draw (E) -- (I);
\draw (F) -- (J);
\draw (F) -- (K);
\draw (G) -- (J);
\draw (H) -- (I);
\draw (H) -- (K);

%Cop arrows
\draw [arrow] (H) -- (E);
\end{tikzpicture}
\centering
$X_4$
}
\end{longtable}
\end{center}
Hence we see that the cops win since $R_3 \subseteq X_4$. We clearly see the importance of controlling the central vertex (the vertex first occupied by $c_1$) to cut the graph into components and force the robber into a smaller area. We see the same method applied in move $X_2$ with $c_2$ and $c_3$.
\end{ex}

For readability, we place two further examples in Appendix \ref{examples-appendix}. We see a similar method in Example \ref{tree-example} whereby the central vertex is controlled to restrict the movement of the robber. In all of these examples we see a strategy employed where cops adjacent to the robber's current position are fixed in place to keep the robber cornered and the behind cops are moved to unoccupied spots to displace the robber and further restrict his movement.
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Implications of Graph Classes}
In this section we present the cop number for some aforementioned classes of graphs through the use of cop strategies.

\begin{defn}
Let $G$ be a graph. $G$ is a \emph{forest} if it has no cycles. $G$ is a \emph{tree} if it is a forest and connected. A \emph{leaf} is a vertex of degree 1 in a forest.
\end{defn}

\begin{defn}
Given $n>0 \in \mathbb{N}$, the \emph{path graph} $P_n$ is the tree of order $n$ such that, for all $v \in V(P_n)$, $d(v) < 3$ \cite{die05}.
\end{defn}

\begin{lem}
For $n>0 \in \mathbb{N}$, $c(P_n) \leq 2$. \label{cn_leq_path}
\end{lem}
\begin{proof}
We will prove this by giving a winning strategy in $\mathcal{CR}(P_n,2)$ for the cop player with the set of cops $\{c_1,c_2\}$. We enumerate $V(P_n) = \{v_1,...v_n\}$ such that, $\{v_\ell,v_m\} \in E(P_n)$ if and only if $m = \ell + 1$, for $1 \leq \ell \leq m \leq n$. If $n=1$, then the cop player can immediately win by declaring $X_1 = V(P_n)$, otherwise the cop player declares $X_1 = \{v_1,v_2\}$. If $n=2$ then the cop player has won, otherwise the cop player may execute the following strategy. Beginning with $c_1$, and alternating between moving $c_1$ and $c_2$, place the cop on the next available vertex with lowest enumeration. With this strategy, it is clear that the cops can win by reducing the order of $P_n$ by one vertex in each move until a cop has been placed on the single vertex the robber will eventually occupy. Hence, for $n>0$, $c(P_n) \leq 2$. In particular, $c(P_1) = 1$ and $c(P_2) = 2$, special cases of a theorem for complete graphs that we will see soon.
\end{proof}
%\begin{proof}
%We first consider $P_1$. The robber player must choose the component consisting of the single vertex $v$ in $P_1$. The cop player may then declare that they will place a single cop on $v$, that is $X_1 = \{v\}$. Hence $V(R_0) = X_1$ so the cop player wins and $c(P_1) = 1$. Now consider $P_2$. Clearly $c(P_2) \neq 1$, since for any $X_i$ chosen by the cop, the robber may then choose $R_{i+1} = P_2 \setminus X_i$, i.e. the robber may choose the vertex in $P_2$ not chosen by the cop in the previous step. Suppose the cop player has two cops. Then, as before with $P_1$, the cop player may select all vertices of $P_2$, leaving the robber with no further components to choose. (In fact, the cop numbers for both $P_1$ and $P_2$ are special cases of a theorem for complete graphs which we will see soon.) To complete this proof we proceed with strong induction. Take $m \in \mathbb{N}$, $m > 2$, such that $c(P_m) \leq 2$ and $c(P_k) \leq 2$, $\forall k \in \mathbb{N}$, with $k < m$. Now consider $P_{m+1}$. The robber must choose the entire graph on his first turn, after which the cop player, given a set of cops $\{c_1,c_2\}$, may place $c_1$ on a vertex of $P_{m+1}$ such that it is split into $P_1$ and $P_{m-2}$, one of which contains the robber. If the robber is in $P_1$, the cop player may win by placing $c_2$ on the single vertex of $P_1$. If not, the cop player places $c_2$ on the vertex of $P_{m-2}$ adjacent to $c_1$, forcing the robber to be captured or inhabit $P_{m-3}$. This process is repeated with alternating cops until the robber is contained in a path $P_\ell$ such that $c(P_\ell) = 1$, after which the cop player may win by fixing the cop previously used for splitting the path and using the second cop to execute the winning strategy for $P_\ell$.
%\end{proof}

\begin{thm}
For $n>1$, $c(P_n) = 2$. \label{cn_path}
\end{thm}
\begin{proof}
We will show that for $n>2$, a single cop is insufficient to catch the robber. Take $\mathcal{CR}(P_n,1)$. As an initial position, we have ($\varnothing,P_n$). Now the cop player selects a vertex of $P_n$ that partitions $P_n$ into two paths. The strategy for the robber, on each selection of $X_i$ by the cop, is to select $R_i$ to be the path with maximum length once $P_n$ is split. Since, for any $P$ an $X_i$-flap of $P_n$, $|V(P)| \geq 1$ and the cop player has no spare cop, there is no strategy for the cop to win, since the robber can continuously choose the larger $X_i$-flap as $P_n$ is connected. Hence $c(P_n) \neq 1$ for $n>1$, so $c(P_n) = 2$ for $n>1$ by Lemma \ref{cn_leq_path}.
\end{proof}

\begin{cor}
$c(C_n) = 3$ for $n>2$. \label{cn_cycle}
\end{cor}
\begin{proof}
First consider $\mathcal{CR}(C_n,2)$. We begin with the initial position $(\varnothing,C_n)$. Any $X_i$ chosen by the cop player splits $C_n$ into two paths. As before, since $C_n$ is connected, the robber player may continuously choose the path with longer length and so will never be caught. So $c(C_n) > 2$. Now consider $\mathcal{CR}(C_n,3)$. We begin again with the position $(\varnothing,C_n)$. The cop player places a cop on an arbitrary vertex $v \in V(C_n)$, so $(X_1,R_1) = (v,C_n \setminus v)$, hence we obtain the graph $P_{n-1}$. By Theorem \ref{cn_path}, $c(P_{n-1}) = 2$, thus, fixing the cop on $v$, there is a winning strategy for the two remaining cops. Hence we have $c(C_n) = 3$.
\end{proof}

\begin{thm}
For $n>0$, $c(K_n) = n$. \label{cn_complete}
\end{thm}
\begin{proof}
We will consider a game containing $n-1$ cops and show that the cop player cannot win. In this game we take the initial position $(\varnothing,K_n)$. The cop player takes their turn and places $n-1$ cops on an arbitrary set of vertices $X_1 \subset V(K_n)$. Now, since $|X_1| = \nolinebreak[4] n-1 \neq \nolinebreak[4] v(K_n)$, the robber player may choose $R_1 = K_n \setminus X_1$. Indeed, since all vertices of $K_n$ are adjacent, the robber may freely choose any vertex not chosen by the cop player, that is $R_i = K_n \setminus X_i$. Hence the robber may avoid capture indefinitely, so $c(K_n) \neq \nolinebreak[4] n-1$. Clearly, since $v(K_n) = n$, $n$ cops may win on the \linebreak first turn, hence $c(K_n) = n$. \\
\end{proof}

We have proven the cop number for specific graph classes, but we can in fact work in the opposite direction. That is, we may describe the properties of graphs that have a given cop number. In subsequent chapters we will introduce stronger ``if and only if'' statements to do precisely this. A natural comparison of the cop number of a graph is to that of its subgraphs. More precisely, we seek to answer the following question.
\begin{center}
\question{Let $G$ and $H$ be graphs with $G$ a subgraph of $H$.}{Is $c(G) \leq c(H)$?}
\end{center}
It would appear that the answer to this question is obvious, since $G$ is in some sense ``simpler'' than $H$ (e.g. has less vertices or edges) and should therefore require no more cops to catch a robber in such a graph. However, our only knowledge of determining the cop number of given graphs so far is through constructing winning strategies for the players. While these are valid proofs, in order to more easily construct a formal proof of the answer to our question, we require an additional property of graphs, described in the following chapter.